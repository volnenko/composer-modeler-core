<?php

use PHPUnit\Framework\TestCase;
use Volnenko\Modeler\Gui\ModelerBuilderURL;

class TestModelerBuilderURL extends TestCase {

    public function testToURL() {
        $result = ModelerBuilderURL::create()->toURL();
        $this->assertEmpty($result);
    }

    public function testUserId() {
        $userId = 'demo';
        $builder = ModelerBuilderURL::create();
        $result = $builder->userId($userId);
        $this->assertNotEmpty($result);
    }

}