<?php

use PHPUnit\Framework\TestCase;
use Volnenko\Modeler\Gui\ModelerBuilderWEB;

class ModelerBuilderWEBTest extends TestCase {

    public function testGetHeader() {
        $result = ModelerBuilderWEB::getHeader();
        $this->assertEmpty($result);
    }

}