<?php

use PHPUnit\Framework\TestCase;
use Volnenko\Modeler\util\ModelerUUID;

class ModelerUUIDTest extends TestCase {

    public function testUUID() {
        $result = ModelerUUID::uuid();
        $this->assertNotEmpty($result);
    }

}