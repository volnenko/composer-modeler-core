<?php namespace Volnenko\Modeler;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class Modeler
{

    /**
     * @var Modeler
     */
    private static $instance = null;

    /**
     * @return Modeler
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

}
