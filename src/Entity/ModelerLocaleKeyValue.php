<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocaleKeyValue extends ModelerAbstractEntity
{

    /**
     * @var string|null
     */
    var $localeId;

    /**
     * @var string|null
     */
    var $localeKeyId;

    /**
     * @var string|null
     */
    var $value;

    /**
     * @return string|null
     */
    public function getLocaleId()
    {
        return $this->localeId;
    }

    /**
     * @param string|null $localeId
     */
    public function setLocaleId($localeId)
    {
        $this->localeId = $localeId;
    }

    /**
     * @return string|null
     */
    public function getLocaleKeyId()
    {
        return $this->localeKeyId;
    }

    /**
     * @param string|null $localeKeyId
     */
    public function setLocaleKeyId($localeKeyId)
    {
        $this->localeKeyId = $localeKeyId;
    }

    /**
     * @return null|string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param null|string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->localeId)) return false;
        if (empty($this->localeKeyId)) return false;
        if (empty($this->value)) return false;
        return true;
    }

}