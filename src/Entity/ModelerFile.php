<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerFile extends ModelerAbstractEntity
{

    /**
     * @var string
     */
    var $fileName;

    /**
     * @var string
     */
    var $extension;

    /**
     * @var string
     */
    var $name;

    /**
     * @var string
     */
    var $description;

    /**
     * @var string
     */
    var $contentType;

    /**
     * @var int
     */
    var $size;

    /**
     * @var string
     */
    var $folderId;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getFolderId()
    {
        return $this->folderId;
    }

    /**
     * @param string $folderId
     */
    public function setFolderId($folderId)
    {
        $this->folderId = $folderId;
    }

    public function getFolder() {
        return null;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->name)) return false;
        return true;
    }

}