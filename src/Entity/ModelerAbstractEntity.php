<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

abstract class ModelerAbstractEntity implements JsonSerializable
{

    /**
     * @var string|null
     */
    var $id;

    /**
     * @var string|null
     */
    var $externalId;

    /**
     * @var integer|null
     */
    var $version;

    /**
     * @var DateTime|null
     */
    var $created;

    /**
     * @var DateTime|null
     */
    var $updated;

    /**
     * @var int
     */
    var $orderIndex;

    /**
     * ModelerAbstractEntity constructor.
     */
    public function __construct()
    {
        ModelerDataUtil::initEntity($this);
    }

    /**
     * @return int
     */
    public function getOrderIndex()
    {
        return $this->orderIndex;
    }

    /**
     * @param int $oderIndex
     */
    public function setOrderIndex($oderIndex)
    {
        $this->orderIndex = $oderIndex;
    }

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return integer|null
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param integer|null $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return DateTime|null
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTime|null $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param DateTime|null $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return null|string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param null|string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function persist() {
        return ModelerEntityManager::getInstance()->persist($this);
    }

    /**
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function merge() {
        return ModelerEntityManager::getInstance()->merge($this);
    }

    /**
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function remove() {
        return ModelerEntityManager::getInstance()->remove($this);
    }

    /**
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function load() {
        return ModelerEntityManager::getInstance()->load($this);
    }

    public function jsonSerialize()
    {
        $result = array();
        foreach ($this as $name => $value) {
            if ($value === null) continue;
            if ($value instanceof DateTime) {
                $result[$name] = $value->format('c');
                continue;
            }
            $check = is_numeric($value) || is_bool($value) || is_string($value);
            if (!$check) continue;
            $result[$name] = $value;
        }
        return $result;
    }

    /**
     * @return array
     */
    public function attributes() {
        $result = array();
        foreach ($this as $name => $value) $result[] = $name;
        return $result;
    }

    /**
     * @param string $attribute
     * @return bool
     */
    public function hasAttribute($attribute) {
        return in_array($attribute, $this->attributes());
    }

    public function xmlMetaSerialize() {
        $result = '';
        $result .= '<entity>'."\n";
        foreach ($this as $name => $value) {
            $type = ModelerDataUtil::getAttributeTypeCode($value);
            if ($type === null) continue;
            $result .= "\t".'<'.$name.'>'.$type.'</'.$name.'>'."\n";
        }
        $result .= '</entity>';
        return $result;
    }

    /**
     * @param string $attribute
     * @return mixed|null
     */
    public function attributeValue($attribute) {
        foreach ($this as $name => $value) if ($name === $attribute) return $value;
        return null;
    }


    public function toMetaXML() {
        return $this->xmlMetaSerialize();
    }

    public function printMetaXML() {
        echo $this->toMetaXML();
    }

    public function toJSON() {
        return ModelerDataUtil::objectToJson($this);
    }

    public function toMetaJSON() {
        return ModelerDataUtil::objectToMetaJson($this);
    }

    public function printJSON() {
        echo $this->toJSON();
    }

    public function printMetaJSON() {
        echo $this->toMetaJSON();
    }

    /**
     * @return ModelerEntityContentAPI
     */
    public function content() {
        return new ModelerEntityContentDAO($this);
    }

}