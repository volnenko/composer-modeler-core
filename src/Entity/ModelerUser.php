<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerUser extends ModelerAbstractEntity
{

    /**
     * @var string|null
     */
    var $login;

    /**
     * @var string|null
     */
    var $passwordHash;

    /**
     * @var string|null
     */
    var $email;

    /**
     * @var string|null
     */
    var $nick;

    /**
     * @var bool|null
     */
    var $locked;

    /**
     * @var string|null
     */
    var $firstName;

    /**
     * @var string|null
     */
    var $lastName;

    /**
     * @var string|null
     */
    var $middleName;

    /**
     * @var DateTime
     */
    var $birthday;

    /**
     * @return DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param DateTime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string|null $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string|null
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string|null $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string|null
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param string|null $passwordHash
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return bool|null
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param bool|null $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return null|string
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * @param null|string $nick
     */
    public function setNick($nick)
    {
        $this->nick = $nick;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->login)) return false;
        return true;
    }

}