<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSettingValue extends ModelerAbstractEntity
{

    /**
     * @var string
     */
    var $settingId;

    /**
     * @var string
     */
    var $settingTypeId;

    /**
     * @var mixed
     */
    var $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getSettingId()
    {
        return $this->settingId;
    }

    /**
     * @param string $settingId
     */
    public function setSettingId($settingId)
    {
        $this->settingId = $settingId;
    }

    /**
     * @return string
     */
    public function getSettingTypeId()
    {
        return $this->settingTypeId;
    }

    /**
     * @param string $settingTypeId
     */
    public function setSettingTypeId($settingTypeId)
    {
        $this->settingTypeId = $settingTypeId;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->settingId)) return false;
        return true;
    }

}