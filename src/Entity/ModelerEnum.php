<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEnum extends ModelerAbstractEntity
{

    /**
     * @var string|null
     */
    var $code;

    /**
     * @var string|null
     */
    var $name;

    /**
     * @var bool
     */
    var $supportSystem = false;

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isSupportSystem()
    {
        return $this->supportSystem;
    }

    /**
     * @param bool $supportSystem
     */
    public function setSupportSystem($supportSystem)
    {
        $this->supportSystem = $supportSystem;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->name)) return false;
        if (empty($this->code)) return false;
        return true;
    }

}