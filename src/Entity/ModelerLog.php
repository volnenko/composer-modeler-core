<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLog extends ModelerAbstractEntity
{

    /**
     * @var string|null
     */
    var $logTypeId;

    /**
     * @var string|null
     */
    var $message;

    /**
     * @return null|string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param null|string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return null|string
     */
    public function getLogTypeId()
    {
        return $this->logTypeId;
    }

    /**
     * @param null|string $logTypeId
     */
    public function setLogTypeId($logTypeId)
    {
        $this->logTypeId = $logTypeId;
    }

    /**
     * @return ModelerLogType|null
     */
    public function getLogType() {
        return ModelerLogType::valueOf($this->logTypeId);
    }

}