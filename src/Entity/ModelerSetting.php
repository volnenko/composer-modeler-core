<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSetting extends ModelerAbstractEntity
{

    /**
     * @var string|null
     */
    var $code;

    /**
     * @var string|null
     */
    var $name;

    /**
     * @var mixed|null
     */
    var $value;

    /**
     * @var string|null
     */
    var $settingTypeId = ModelerSettingType::STRING;

    /**
     * @var string|null
     */
    var $settingValueTypeId = ModelerSettingValueType::SINGLE;

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getSettingTypeId()
    {
        return $this->settingTypeId;
    }

    /**
     * @param null|string $settingTypeId
     */
    public function setSettingTypeId($settingTypeId)
    {
        $this->settingTypeId = $settingTypeId;
    }

    /**
     * @return ModelerSettingType|null
     */
    public function getSettingType() {
        return ModelerSettingType::valueOf($this->settingTypeId);
    }

    /**
     * @param ModelerSettingType $settingType
     */
    public function setSettingType(ModelerSettingType $settingType) {
        if ($settingType === null) return;
        $this->settingTypeId = $settingType->getCode();
    }

    /**
     * @return null|string
     */
    public function getSettingValueTypeId()
    {
        return $this->settingValueTypeId;
    }

    /**
     * @param null|string $settingValueTypeId
     */
    public function setSettingValueTypeId($settingValueTypeId)
    {
        $this->settingValueTypeId = $settingValueTypeId;
    }

    /**
     * @return mixed|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed|null $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return bool
     */
    public function isSingle() {
        return $this->settingValueTypeId === ModelerSettingValueType::SINGLE;
    }

    /**
     * @return bool
     */
    public function isMultiple() {
        return $this->settingValueTypeId === ModelerSettingValueType::MULTIPLE;
    }

    /**
     * @return bool
     */
    public function isInteger() {
        return $this->settingTypeId === ModelerSettingType::INTEGER;
    }

    /**
     * @return bool
     */
    public function isLong() {
        return $this->settingTypeId === ModelerSettingType::LONG;
    }

    /**
     * @return bool
     */
    public function isFloat() {
        return $this->settingTypeId === ModelerSettingType::FLOAT;
    }

    /**
     * @return bool
     */
    public function isDouble() {
        return $this->settingTypeId === ModelerSettingType::DOUBLE;
    }

    /**
     * @return bool
     */
    public function isDate() {
        return $this->settingTypeId === ModelerSettingType::DATE;
    }

    /**
     * @return bool
     */
    public function isBoolean() {
        return $this->settingTypeId === ModelerSettingType::BOOLEAN;
    }

    /**
     * @return bool
     */
    public function isString() {
        return $this->settingTypeId === ModelerSettingType::STRING;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->code)) return false;
        return true;
    }

}