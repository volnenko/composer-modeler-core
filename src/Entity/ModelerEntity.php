<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEntity extends ModelerAbstractEntity
{

    /**
     * @var string|null
     */
    var $name;

    /**
     * @var string|null
     */
    var $code;

    /**
     * @var bool
     */
    var $supportSystem = false;

    /**
     * @var string
     */
    var $sortField = ModelerAttributeConst::ORDER_INDEX;

    /**
     * @var string
     */
    var $sortTypeId = ModelerSortType::DESC;

    /**
     * @var string
     */
    var $settingValueTypeId = ModelerSettingValueType::SINGLE;

    /**
     * @return string
     */
    public function getSortField()
    {
        return $this->sortField;
    }

    /**
     * @param string $sortField
     */
    public function setSortField($sortField)
    {
        $this->sortField = $sortField;
    }

    /**
     * @return string
     */
    public function getSortTypeId()
    {
        return $this->sortTypeId;
    }

    /**
     * @param string $sortTypeId
     */
    public function setSortTypeId($sortTypeId)
    {
        $this->sortTypeId = $sortTypeId;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return bool
     */
    public function getSupportSystem()
    {
        return $this->supportSystem;
    }

    /**
     * @param bool $supportSystem
     */
    public function setSupportSystem($supportSystem)
    {
        $this->supportSystem = $supportSystem;
    }

    /**
     * @return string
     */
    public function getSettingValueTypeId()
    {
        return $this->settingValueTypeId;
    }

    /**
     * @param string $settingValueTypeId
     */
    public function setSettingValueTypeId($settingValueTypeId)
    {
        $this->settingValueTypeId = $settingValueTypeId;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->name)) return false;
        if (empty($this->code)) return false;
        return true;
    }

}