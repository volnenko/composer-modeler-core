<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocaleKey extends ModelerAbstractEntity
{

    /**
     * @var string|null
     */
    var $localeId;

    /**
     * @var string|null
     */
    var $name;

    /**
     * @return string|null
     */
    public function getLocaleId()
    {
        return $this->localeId;
    }

    /**
     * @param string|null $localeId
     */
    public function setLocaleId($localeId)
    {
        $this->localeId = $localeId;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->name)) return false;
        if (empty($this->localeId)) return false;
        return true;
    }

}