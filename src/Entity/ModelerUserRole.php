<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerUserRole extends ModelerAbstractEntity
{

    /**
     * @var string|null
     */
    var $userId;

    /**
     * @var string|null
     */
    var $roleId;

    /**
     * @return string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string|null $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string|null
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * @param string|null $roleId
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;
    }


    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->userId)) return false;
        if (empty($this->roleId)) return false;
        return true;
    }

}