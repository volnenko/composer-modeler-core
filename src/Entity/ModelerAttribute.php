<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerAttribute extends ModelerAbstractEntity
{

    /**
     * @var string|null
     */
    var $name;

    /**
     * @var string|null
     */
    var $code;

    /**
     * @var string|null
     */
    var $attributeTypeId;

    /**
     * @var string|null
     */
    var $enumId;

    /**
     * @var string|null
     */
    var $entityId;

    /**
     * @var int|null
     */
    var $columnWidth;

    /**
     * @var bool
     */
    var $supportSystem = false;


    /**
     * @return bool
     */
    public function isSupportSystem()
    {
        return $this->supportSystem;
    }

    /**
     * @param bool $supportSystem
     */
    public function setSupportSystem($supportSystem)
    {
        $this->supportSystem = $supportSystem;
    }

    /**
     * @return int|null
     */
    public function getColumnWidth()
    {
        return $this->columnWidth;
    }

    /**
     * @param int|null $columnWidth
     */
    public function setColumnWidth($columnWidth)
    {
        $this->columnWidth = $columnWidth;
    }

    /**
     * @return null|string
     */
    public function getEnumId()
    {
        return $this->enumId;
    }

    /**
     * @param null|string $enumId
     */
    public function setEnumId($enumId)
    {
        $this->enumId = $enumId;
    }


    /**
     * @return null|string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param null|string $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return null|string
     */
    public function getAttributeTypeId()
    {
        return $this->attributeTypeId;
    }

    /**
     * @param null|string $attributeTypeId
     */
    public function setAttributeTypeId($attributeTypeId)
    {
        $this->attributeTypeId = $attributeTypeId;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->name)) return false;
        if (empty($this->code)) return false;
        return true;
    }

}