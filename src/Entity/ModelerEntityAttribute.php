<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEntityAttribute extends ModelerAbstractEntity
{
    /**
     * @var string|null
     */
    var $entityId;

    /**
     * @var string|null
     */
    var $attributeId;

    /**
     * @var bool
     */
    var $supportDisplayName;

    /**
     * @var bool
     */
    var $supportSearchField;

    /**
     * @var bool
     */
    var $supportHideSelect;

    /**
     * @var bool
     */
    var $supportHideInsert;

    /**
     * @var bool
     */
    var $supportHideUpdate;

    /**
     * @var bool
     */
    var $supportHideRemove;

    /**
     * @return bool
     */
    public function isSupportDisplayName()
    {
        return $this->supportDisplayName;
    }

    /**
     * @param bool $supportDisplayName
     */
    public function setSupportDisplayName($supportDisplayName)
    {
        $this->supportDisplayName = $supportDisplayName;
    }

    /**
     * @return bool
     */
    public function isSupportSearchField()
    {
        return $this->supportSearchField;
    }

    /**
     * @param bool $supportSearchField
     */
    public function setSupportSearchField($supportSearchField)
    {
        $this->supportSearchField = $supportSearchField;
    }

    /**
     * @return bool
     */
    public function isSupportHideSelect()
    {
        return $this->supportHideSelect;
    }

    /**
     * @param bool $supportHideSelect
     */
    public function setSupportHideSelect($supportHideSelect)
    {
        $this->supportHideSelect = $supportHideSelect;
    }

    /**
     * @return bool
     */
    public function isSupportHideInsert()
    {
        return $this->supportHideInsert;
    }

    /**
     * @param bool $supportHideInsert
     */
    public function setSupportHideInsert($supportHideInsert)
    {
        $this->supportHideInsert = $supportHideInsert;
    }

    /**
     * @return bool
     */
    public function isSupportHideUpdate()
    {
        return $this->supportHideUpdate;
    }

    /**
     * @param bool $supportHideUpdate
     */
    public function setSupportHideUpdate($supportHideUpdate)
    {
        $this->supportHideUpdate = $supportHideUpdate;
    }

    /**
     * @return bool
     */
    public function isSupportHideRemove()
    {
        return $this->supportHideRemove;
    }

    /**
     * @param bool $supportHideRemove
     */
    public function setSupportHideRemove($supportHideRemove)
    {
        $this->supportHideRemove = $supportHideRemove;
    }

    /**
     * @return string|null
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string|null $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string|null
     */
    public function getAttributeId()
    {
        return $this->attributeId;
    }

    /**
     * @param string|null $attributeId
     */
    public function setAttributeId($attributeId)
    {
        $this->attributeId = $attributeId;
    }

    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->entityId)) return false;
        if (empty($this->attributeId)) return false;
        return true;
    }

}