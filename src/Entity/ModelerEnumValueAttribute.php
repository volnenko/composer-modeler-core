<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEnumValueAttribute extends ModelerAbstractEntity
{

    /**
     * @var string
     */
    var $enumId;

    /**
     * @var string
     */
    var $attributeId;

    /**
     * @var mixed
     */
    var $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getEnumId()
    {
        return $this->enumId;
    }

    /**
     * @param string $enumId
     */
    public function setEnumId($enumId)
    {
        $this->enumId = $enumId;
    }

    /**
     * @return string
     */
    public function getAttributeId()
    {
        return $this->attributeId;
    }

    /**
     * @param string $attributeId
     */
    public function setAttributeId($attributeId)
    {
        $this->attributeId = $attributeId;
    }

}