<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEnumValue extends ModelerAbstractEntity
{

    /**
     * @var string
     */
    var $code;

    /**
     * @var string
     */
    var $name;

    /**
     * @var string
     */
    var $enumId;

    /**
     * @var bool
     */
    var $supportSystem = false;

    /**
     * @return bool
     */
    public function getSupportSystem()
    {
        return $this->supportSystem;
    }

    /**
     * @param bool $supportSystem
     */
    public function setSupportSystem($supportSystem)
    {
        $this->supportSystem = $supportSystem;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEnumId()
    {
        return $this->enumId;
    }

    /**
     * @param string $enumId
     */
    public function setEnumId($enumId)
    {
        $this->enumId = $enumId;
    }

    /**
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function getEnum() {
        return ModelerEnumDAO::getInstance()->findOne($this->enumId);
    }

    /**
     * @param ModelerEnum $enum
     */
    public function setEnum(ModelerEnum $enum) {
        if ($enum === null) return;
        $this->enumId = $enum->getId();
    }


    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->enumId)) return false;
        if (empty($this->code)) return false;
        return true;
    }

}