<?php namespace Volnenko\Modeler\Entity;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEnumAttribute extends ModelerAbstractEntity
{

    /**
     * @var string
     */
    var $enumId;

    /**
     * @var string
     */
    var $attributeId;

    /**
     * @return string
     */
    public function getEnumId()
    {
        return $this->enumId;
    }

    /**
     * @param string $enumId
     */
    public function setEnumId($enumId)
    {
        $this->enumId = $enumId;
    }

    /**
     * @return string
     */
    public function getAttributeId()
    {
        return $this->attributeId;
    }

    /**
     * @param string $attributeId
     */
    public function setAttributeId($attributeId)
    {
        $this->attributeId = $attributeId;
    }


    /**
     * @return bool
     */
    public function isValid() {
        if (empty($this->id)) return false;
        if (empty($this->enumId)) return false;
        if (empty($this->attributeId)) return false;
        return true;
    }

}