<?php namespace Volnenko\Modeler\Locale;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocalizationValue
{

    /**
     * @var ModelerLocalization
     */
    var $localization;

    /**
     * @var ModelerLocalizationLocale
     */
    var $locale;

    /**
     * @var ModelerLocalizationKey
     */
    var $key;

    /**
     * @var string
     */
    var $value;

    /**
     * ModelerLocalizationValue constructor.
     * @param ModelerLocalization $localization
     * @param ModelerLocalizationLocale $locale
     * @param ModelerLocalizationKey $key
     * @param string $value
     */
    public function __construct(ModelerLocalization $localization, ModelerLocalizationLocale $locale, ModelerLocalizationKey $key, $value)
    {
        $this->localization = $localization;
        $this->locale = $locale;
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @param string $name
     * @param string $code
     * @return ModelerLocalizationLocale
     */
    public function locale($name, $code) {
        return $this->locale->locale($name, $code);
    }

    /**
     * @param string $name
     * @return ModelerLocalizationKey
     */
    public function key($name) {
        return $this->locale->key($name);
    }

    /**
     * @param string $value
     * @return ModelerLocalizationValue
     */
    public function value($value) {
        return $this->key->value($value);
    }

    public function apply() {
        $this->localization->apply();
    }


}