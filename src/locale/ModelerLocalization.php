<?php namespace Volnenko\Modeler\Locale;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocalization
{

    /**
     * @var ModelerLocalization
     */
    private static $instance = null;

    /**
     * @return ModelerLocalization
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @var ModelerLocale[]
     */
    var $locales = array();

    /**
     * @param string $name
     * @param string $code
     * @return ModelerLocalizationLocale
     */
    public function locale($name, $code) {
        $locale = new ModelerLocalizationLocale($this, $name, $code);
        $this->locales[] = $locale;
        return $locale;
    }

    public function apply() {

    }

}