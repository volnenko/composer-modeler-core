<?php namespace Volnenko\Modeler\Locale;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocalizationKey
{

    /**
     * @var ModelerLocalization
     */
    var $localization;

    /**
     * @var ModelerLocalizationLocale
     */
    var $locale;

    /**
     * @var string
     */
    var $name;

    /**
     * @var ModelerLocalizationValue[]
     */
    var $values;

    /**
     * ModelerLocalizationKey constructor.
     * @param ModelerLocalization $localization
     * @param ModelerLocalizationLocale $locale
     * @param string $name
     */
    public function __construct(
        ModelerLocalization $localization,
        ModelerLocalizationLocale $locale,
        $name
    )
    {
        $this->localization = $localization;
        $this->locale = $locale;
        $this->name = $name;
    }

    /**
     * @param $name
     * @return ModelerLocalizationKey
     */
    public function key($name) {
        return $this->locale->key($name);
    }

    /**
     * @param string $value
     * @return ModelerLocalizationValue
     */
    public function value($value) {
        $result = new ModelerLocalizationValue($this->localization, $this->locale, $this, $value);
        $this->values[] = $result;
        return $result;
    }

    public function apply() {
        $this->localization->apply();
    }

}