<?php namespace Volnenko\Modeler\Locale;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocalizationLocale
{

    /**
     * @var ModelerLocalization
     */
    private $localization;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var ModelerLocalizationKey[]
     */
    private $keys = array();

    /**
     * ModelerLocalizationLocale constructor.
     * @param ModelerLocalization $localization
     * @param string $name
     * @param string $code
     */
    public function __construct(
        ModelerLocalization $localization,
        $name, $code
    )
    {
        $this->localization = $localization;
        $this->name = $name;
        $this->code = $code;
    }

    /**
     * @param string $name
     * @param string $code
     * @return ModelerLocalizationLocale
     */
    public function locale($name, $code) {
        return $this->localization->locale($name, $code);
    }

    /**
     * @param string $name
     * @return ModelerLocalizationKey
     */
    public function key($name) {
        $key = new ModelerLocalizationKey($this->localization, $this, $name);
        $this->keys[] = $key;
        return $key;
    }

    public function apply() {
        $this->localization->apply();
    }

}