<?php namespace Volnenko\Modeler\Api;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

interface ModelerDataAccessAPI {
    /**
     * @param ModelerAbstractEntity|null $entity
     * @return ModelerAbstractEntity|null
     */
    public function persist(ModelerAbstractEntity $entity);

    /**
     * @param ModelerAbstractEntity|null $entity
     * @return ModelerAbstractEntity|null
     */
    public function merge(ModelerAbstractEntity $entity);

    /**
     * @param ModelerAbstractEntity|null $entity
     * @return ModelerAbstractEntity|null
     */
    public function remove(ModelerAbstractEntity $entity);

    /**
     * @param ModelerAbstractEntity|null $entity
     * @return ModelerAbstractEntity|null
     */
    public function refresh(ModelerAbstractEntity $entity);

    /**
     * @param string $id
     * @return mixed
     */
    public function findOne($id);

    /**
     * @param string $attribute
     * @param mixed $value
     */
    public function findOneByAttribute($attribute, $value);

    /**
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     */
    public function findAll($start = null, $limit = null, $sortAttribute = null, $sortTypeId = null);

    /**
     * @param string $findAttribute
     * @param mixed $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     */
    public function findAllByAttribute($findAttribute, $findValue, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null);

    /**
     * @param string $findAttribute
     * @param mixed $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     */
    public function likeAllByAttribute($findAttribute, $findValue, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null);

    /**
     * @param string $id
     * @return mixed
     */
    public function exists($id);

    /**
     * @return int
     */
    public function count();

    /**
     * @return array
     */
    public function ids();

    /**
     * @param string $id
     * @return mixed
     */
    public function removeById($id);

    /**
     * @param string $attribute
     * @param mixed $value
     * @return array
     */
    public function removeAllByAttribute($attribute, $value);

    /**
     * @return void
     */
    public function removeAll();

    /**
     * @return boolean
     */
    public function isEmpty();

    /**
     * @return ModelerCriteria
     */
    public function criteria();

    /**
     * @return ModelerContentAPI
     */
    public function content();

}