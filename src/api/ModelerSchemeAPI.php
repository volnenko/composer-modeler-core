<?php namespace Volnenko\Modeler\Api;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

interface ModelerSchemeAPI {

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeString($code, $name);

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeInteger($code, $name);

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeLong($code, $name);

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeDouble($code, $name);

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeFloat($code, $name);

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeBoolean($code, $name);

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeDateTime($code, $name);

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeEntity
     */
    public function entity($code, $name);

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeEnum
     */
    public function enum($code, $name);

    public function store();

    public function system();

}