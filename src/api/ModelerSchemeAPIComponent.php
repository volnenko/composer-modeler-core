<?php namespace Volnenko\Modeler\Api;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

interface ModelerSchemeAPIComponent extends ModelerSchemeAPI {

    /**
     * @return ModelerScheme
     */
    public function scheme();

    public function flush();

}