<?php namespace Volnenko\Modeler\Api;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

interface ModelerContentAPI {

    public function get($id);

    public function set($id, $content);

    public function exists($id);

    public function remove($id);

}