<?php namespace Volnenko\Modeler\Api;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

interface ModelerEntityContentAPI {

    public function get();

    public function set($content);

    public function exists();

    public function remove();


}