<?php namespace Volnenko\Modeler\Api;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

interface ModelerChildAPI {

    public function findOne($id);

    public function findAll($id);

    public function ids($id);

    public function count($id);

    public function clear($id);

    public function add($id, $childId);

    public function set($id, $childId);

    public function get($id, $childId);

    public function remove($id, $childId);

    public function exist($id, $childId);

}