<?php namespace Volnenko\Modeler\Constant;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerStorageConst {

    const FOLDER_STORAGE = '/storage';

}