<?php namespace Volnenko\Modeler\Constant;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSettingConst {

    const SECRET_KEY = 'SECRET_KEY';

    const PAGE_LIMIT = 'PAGE_LIMIT';

    const SITE_NAME = 'SITE_NAME';

    const PASSWORD_SALT = 'PASSWORD_SALT';

    const PASSWORD_HASH_COUNT = 'PASSWORD_HASH_COUNT';

}