<?php namespace Volnenko\Modeler\Constant;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerBooleanConst {

    const TRUE = 'true';

    const FALSE = 'false';

    public static function parseString($value) {
        return self::TRUE === $value;
    }

}