<?php namespace Volnenko\Modeler\Constant;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerAttributeConst {

    const ID = 'id';

    const OPERATION = 'operation';

    const NAME = 'name';

    const XTYPE = 'xtype';

    const CODE = 'code';

    const LOGIN = 'login';

    const EMAIL = 'email';

    const VALUE = 'value';

    const LIMIT = 'limit';

    const START = 'start';

    const TIME = 'time';

    const TYPE = 'type';

    const IP = 'ip';

    const USER_ID = 'userId';

    const HOST = 'host';

    const FILE_NAME = 'fileName';

    const SIZE = 'size';

    const LOCKED = 'locked';

    const SUPPORT_DISPLAY_NAME = 'supportDisplayName';

    const SUPPORT_SEARCH_FIELD = 'supportSearchField';

    const TMP_NAME = 'tmp_name';

    const CONTENT_TYPE = 'contentType';

    const DESCRIPTION = 'description';

    const SUPPORT_SYSTEM = 'supportSystem';

    const FOLDER_ID = 'folderId';

    const IMAGE_ID = 'imageId';

    const FIND_VALUE = 'findValue';

    const ORDER_INDEX = 'orderIndex';

    const EXTERNAL_ID = 'externalId';

    const COLUMN_WIDTH = 'columnWidth';

    const SORT_TYPE = 'sortType';

    const SORT_TYPE_ID = 'sortTypeId';

    const SORT_FIELD = 'sortField';

    const ENTITY_ID = 'entityId';

    const ENTITY_CLASS = 'entityClass';

    const ENUM_ID = 'enumId';

    const ATTRIBUTE_ID = 'attributeId';

    const ATTRIBUTE_TYPE_ID = 'attributeTypeId';

    const SETTING_TYPE_ID = 'settingTypeId';

    const SETTING_VALUE_TYPE_ID = 'settingValueTypeId';

    const PASSWORD = 'password';

    const PASSWORD_HASH = 'passwordHash';

    const STATISTIC = 'statistic';

    const LOCALE_ID = 'localeId';

    const LOCALE_KEY_ID = 'localeKeyId';

    const LOCALE_KEY_VALUE_ID = 'localeKeyValueId';

    const SETTING_ID = 'settingId';

    const ROLE_ID = 'roleId';

    const SESSION_ID = 'sessionId';

}