<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEntityContentDAO implements ModelerEntityContentAPI
{

    /**
     * @var ModelerAbstractEntity
     */
    private $entity;

    /**
     * @var null|string
     */
    private $entityName;

    /**
     * @var ModelerContentDAO
     */
    private $contentDAO;

    /**
     * ModelerEntityContentDAO constructor.
     * @param ModelerAbstractEntity $entity
     */
    public function __construct(ModelerAbstractEntity $entity)
    {
        $this->entity = $entity;
        $this->entityName = ModelerStorageManager::getInstance()->getEntityName($this->entity);
        $this->contentDAO = new ModelerContentDAO($this->entityName);
    }

    public function get()
    {
        return $this->contentDAO->get($this->entity->id);
    }

    public function set($content)
    {
        $this->contentDAO->set($this->entity->id, $content);
    }

    public function exists()
    {
        return $this->contentDAO->exists($this->entity->id);
    }

    public function remove()
    {
        $this->contentDAO->remove($this->entity->id);
    }

}