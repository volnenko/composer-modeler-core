<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocaleKeyValueDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerLocaleKeyValueDAO
     */
    private static $instance = null;

    /**
     * @return ModelerLocaleKeyValueDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerLocaleKeyValue::class);
        return self::$instance;
    }

}