<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocaleDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerLocaleDAO
     */
    private static $instance = null;

    /**
     * @return ModelerLocaleDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerLocale::class);
        return self::$instance;
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerLocale
     */
    public static function addLocale($code, $name) {
        $locale = new ModelerLocale();
        $locale->setName($name);
        $locale->setCode($code);
        return $locale;
    }

    /**
     * @param $code
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findByCode($code) {
        return $this->criteria()->predicateAnd()->conditionEquals(ModelerAttributeConst::CODE, $code)->findOne();
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerLocale[]
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->conditionContains(ModelerAttributeConst::CODE, $findValue)
            ->findAll($start, $limit);
    }

}