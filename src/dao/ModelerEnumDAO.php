<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEnumDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerEnumDAO
     */
    private static $instance = null;

    /**
     * @return ModelerEnumDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerEnum::class);
        return self::$instance;
    }

    /**
     * @param $code
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findByCode($code) {
        return $this->criteria()->predicateAnd()->conditionEquals(ModelerAttributeConst::CODE, $code)->findOne();
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->conditionContains(ModelerAttributeConst::CODE, $findValue)
            ->findAll($start, $limit);
    }

}