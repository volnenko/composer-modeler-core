<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerUserRoleDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerUserRoleDAO
     */
    private static $instance = null;

    /**
     * @return ModelerUserRoleDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerUserRole::class);
        return self::$instance;
    }

    /**
     * @param string $userId
     * @param string $roleId
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findOneByUserIdRoleId($userId, $roleId) {
        return $this->criteria()
            ->setLimit(1)
            ->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::USER_ID, $userId)
            ->conditionEquals(ModelerAttributeConst::ROLE_ID, $roleId)
            ->findOne();
    }

    /**
     * @param $userId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAllByUserId($userId) {
        return $this->criteria()
            ->sort(ModelerAttributeConst::USER_ID, ModelerSortType::DESC)
            ->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::USER_ID, $userId)
            ->findAll();
    }

    /**
     * @param string $userId
     * @param string $roleId
     * @return ModelerAbstractEntity|ModelerUserRole
     * @throws ModelerAbstractEntityException
     */
    public function mergeUserIdRoleId($userId, $roleId) {
        $userRole = $this->findOneByUserIdRoleId($userId, $roleId);
        if ($userRole !== null) return $userRole;
        $userRole = new ModelerUserRole();
        $userRole->userId = $userId;
        $userRole->roleId = $roleId;
        return $userRole->merge();
    }

}