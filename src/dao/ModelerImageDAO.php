<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerImageDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerImageDAO
     */
    private static $instance = null;

    /**
     * @return ModelerImageDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerImage::class);
        return self::$instance;
    }

    /**
     * @param string $folderId
     * @param int $start
     * @param int $limit
     * @param string $sortField
     * @param string $sortTypeId
     * @return ModelerImage[]
     * @throws ModelerAbstractEntityException
     */
    public function findAllByFolderId(
        $folderId, $start = null, $limit = null,
        $sortField = ModelerAttributeConst::ORDER_INDEX,
        $sortTypeId = ModelerSortType::DESC
    ) {
        return $this
            ->criteria()
            ->sort($sortField, $sortTypeId)
            ->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::FOLDER_ID, $folderId)
            ->findAll($start, $limit);
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerImage[]
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->conditionContains(ModelerAttributeConst::DESCRIPTION, $findValue)
            ->findAll($start, $limit);
    }

    /**
     * @param string $findValue
     * @param string $folderId
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerImage[]
     * @throws ModelerAbstractEntityException
     */
    public function likeByFolderId(
        $findValue, $folderId, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        if (empty($folderId)) return $this->likeByFindValue($findValue, $start, $limit, $sortAttribute, $sortTypeId);
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::FOLDER_ID, $folderId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->conditionContains(ModelerAttributeConst::DESCRIPTION, $findValue)
            ->findAll($start, $limit);
    }

}