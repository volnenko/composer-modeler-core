<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSessionDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerSessionDAO
     */
    private static $instance = null;

    /**
     * @return ModelerSessionDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerSession::class);
        return self::$instance;
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerSession[]
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::ID, $findValue)
            ->conditionContains(ModelerAttributeConst::IP, $findValue)
            ->conditionContains(ModelerAttributeConst::HOST, $findValue)
            ->findAll($start, $limit);
    }

}