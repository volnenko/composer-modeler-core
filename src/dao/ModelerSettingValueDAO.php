<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSettingValueDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerSettingValueDAO
     */
    private static $instance = null;

    /**
     * @return ModelerSettingValueDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerSettingValue::class);
        return self::$instance;
    }

}