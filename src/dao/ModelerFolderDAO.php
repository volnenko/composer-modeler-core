<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerFolderDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerFolderDAO
     */
    private static $instance = null;

    /**
     * @return ModelerFolderDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerFolder::class);
        return self::$instance;
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerFolder[]
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->conditionContains(ModelerAttributeConst::DESCRIPTION, $findValue)
            ->findAll($start, $limit);
    }

}