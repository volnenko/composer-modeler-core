<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocaleKeyDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerLocaleKeyDAO
     */
    private static $instance = null;

    /**
     * @return ModelerLocaleKeyDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerLocaleKey::class);
        return self::$instance;
    }

    /**
     * @param string $localeId
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerLocaleKey[]
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $localeId, $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::LOCALE_ID, $localeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->findAll($start, $limit);
    }

}