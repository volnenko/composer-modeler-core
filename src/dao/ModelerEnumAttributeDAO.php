<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEnumAttributeDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerEnumAttributeDAO
     */
    private static $instance = null;

    /**
     * @return ModelerEnumAttributeDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerEnumAttribute::class);
        return self::$instance;
    }

    /**
     * @param $enumId
     * @param $attributeId
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findOneByEnumAttribute($enumId, $attributeId) {
        return $this->criteria()->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::ENUM_ID, $enumId)
            ->conditionEquals(ModelerAttributeConst::ATTRIBUTE_ID, $attributeId)
            ->findOne();
    }

    /**
     * @param string $enumId
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAllByEnumId($enumId, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::ENUM_ID, $enumId)
            ->findAll($start, $limit);
    }

}