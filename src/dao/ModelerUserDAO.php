<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerUserDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerUserDAO
     */
    private static $instance = null;

    /**
     * @return ModelerUserDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerUser::class);
        return self::$instance;
    }

    /**
     * @param $userId
     * @return ModelerUser
     * @throws ModelerAbstractEntityException
     */
    public function getUserById($userId)
    {
        return $this->findOne($userId);
    }

    /**
     * @param $login
     * @return ModelerUser
     * @throws ModelerAbstractEntityException
     */
    public function getUserByLogin($login)
    {
        return $this->findOneByAttribute(ModelerAttributeConst::LOGIN, $login);
    }

    /**
     * @param $email
     * @return ModelerUser
     * @throws ModelerAbstractEntityException
     */
    public function getUserByEmail($email)
    {
        return $this->findOneByAttribute( ModelerAttributeConst::EMAIL, $email);
    }

    /**
     * @param $login
     * @return ModelerUser
     * @throws ModelerAbstractEntityException
     * @throws ModelerLoginEmptyException
     */
    public function createUserByLogin($login)
    {
        if (empty($login)) throw new ModelerLoginEmptyException();
        $user = new ModelerUser();
        $user->setLogin($login);
        $this->em->persist($user);
        return $user;
    }

    /**
     * @param $login
     * @param $password
     * @param $email
     * @return ModelerUser
     * @throws ModelerAbstractEntityException
     * @throws ModelerEmailEmptyException
     * @throws ModelerLoginEmptyException
     * @throws ModelerPasswordEmptyException
     */
    public function createUser($login, $password, $email)
    {
        if (empty($login)) throw new ModelerLoginEmptyException();
        if (empty($password)) throw new ModelerPasswordEmptyException();
        if (empty($email)) throw new ModelerEmailEmptyException();
        $passwordHash = ModelerPasswordUtil::getPasswordHash($password);
        $user = new ModelerUser();
        $user->setLogin($login);
        $user->setEmail($email);
        $user->setPasswordHash($passwordHash);
        $this->em->persist($user);
        return $user;
    }

    /**
     * @param ModelerUser $user
     * @return ModelerUser|null
     * @throws ModelerAbstractEntityException
     */
    public function lockUser(ModelerUser $user) {
        if ($user === null) return null;
        $user->setLocked(true);
        $this->em->merge($user);
        return $user;
    }

    /**
     * @param ModelerUser $user
     * @return null|ModelerUser
     * @throws ModelerAbstractEntityException
     */
    public function unlockUser(ModelerUser $user) {
        if ($user === null) return null;
        $user->setLocked(false);
        $this->em->merge($user);
        return $user;
    }

    /**
     * @param $userId
     * @return ModelerUser|null
     * @throws ModelerAbstractEntityException
     */
    public function lockUserById($userId) {
        $user = $this->getUserById($userId);
        return $this->lockUser($user);
    }

    /**
     * @param $userId
     * @return null|ModelerUser
     * @throws ModelerAbstractEntityException
     */
    public function unlockUserById($userId) {
        $user = $this->getUserById($userId);
        return $this->unlockUser($user);
    }

    /**
     * @param $login
     * @return ModelerUser|null
     * @throws ModelerAbstractEntityException
     */
    public function lockUserByLogin($login) {
        $user = $this->getUserByLogin($login);
        return $this->lockUser($user);
    }

    /**
     * @param $login
     * @return null|ModelerUser
     * @throws ModelerAbstractEntityException
     */
    public function unlockUserByLogin($login) {
        $user = $this->getUserByLogin($login);
        return $this->unlockUser($user);
    }

    /**
     * @param $email
     * @return ModelerUser|null
     * @throws ModelerAbstractEntityException
     */
    public function lockUserByEmail($email) {
        $user = $this->getUserByEmail($email);
        return $this->lockUser($user);
    }

    /**
     * @param $email
     * @return ModelerUser|null
     * @throws ModelerAbstractEntityException
     */
    public function unlockUserByEmail($email) {
        $user = $this->getUserByEmail($email);
        return $this->unlockUser($user);
    }

    /**
     * @param string|null $userId
     */
    public function removeUserById($userId)
    {
        $this->removeById($userId);
    }

    /**
     * @param string $login
     * @throws ModelerAbstractEntityException
     */
    public function removeUserByLogin($login)
    {
        $this->removeAllByAttribute(ModelerAttributeConst::LOGIN, $login);
    }

    /**
     * @param string $email
     * @throws ModelerAbstractEntityException
     */
    public function removeUserByEmail($email)
    {
        $this->removeAllByAttribute(ModelerAttributeConst::EMAIL, $email);
    }

    /**
     * @param string $login
     * @return bool
     * @throws ModelerAbstractEntityException
     */
    public function isLoginUnique($login) {
        $user = $this->getUserByLogin($login);
        return $user === null;
    }

    /**
     * @param string $email
     * @return bool
     * @throws ModelerAbstractEntityException
     */
    public function isEmailUnique($email) {
        $user = $this->getUserByEmail($email);
        return $user === null;
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::LOGIN, $findValue)
            ->conditionContains(ModelerAttributeConst::EMAIL, $findValue)
            ->findAll($start, $limit);
    }

}