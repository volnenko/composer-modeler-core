<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerAttributeDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerAttributeDAO
     */
    private static $instance = null;

    /**
     * @return ModelerAttributeDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerAttribute::class);
        return self::$instance;
    }

    /**
     * @param $code
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findByCode($code) {
        return $this->criteria()->predicateAnd()->conditionEquals(ModelerAttributeConst::CODE, $code)->findOne();
    }

    /**
     * @param string $entityClass
     * @return ModelerEntity[]
     * @throws ModelerAbstractEntityException
     */
    public function findAllByEntityClass($entityClass) {
        $entity = ModelerEntityDAO::getInstance()->findByCode($entityClass);
        if ($entity === null) return array();
        $entityId = $entity->getId();
        return $this->findAllByEntityId($entityId);
    }

    /**
     * @param string $entityId
     * @return ModelerEntity[]
     * @throws ModelerAbstractEntityException
     */
    public function findAllByEntityId($entityId) {
        if (empty($entityId)) return array();
        $entityAttributes = ModelerEntityAttributeDAO::getInstance()->findAllByEntity($entityId);
        if (empty($entityAttributes)) return array();
        $result = array();
        foreach ($entityAttributes as $entityAttribute) {
            if ($entityAttribute === null) continue;
            $attributeId = $entityAttribute->getAttributeId();
            if (empty($attributeId)) continue;
            $attribute = ModelerAttributeDAO::getInstance()->findOne($attributeId);
            if ($attribute === null) continue;
            $result[] = $attribute;
        }
        return $result;
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->conditionContains(ModelerAttributeConst::CODE, $findValue)
            ->findAll($start, $limit);
    }

    /**
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAllSimple(
        $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionNotEquals(ModelerAttributeConst::ATTRIBUTE_TYPE_ID, ModelerAttributeType::ENUM)
            ->conditionNotEquals(ModelerAttributeConst::ATTRIBUTE_TYPE_ID, ModelerAttributeType::ENTITY)
            ->findAll($start, $limit);
    }

}