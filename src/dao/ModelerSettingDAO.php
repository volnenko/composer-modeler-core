<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSettingDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerSettingDAO
     */
    private static $instance = null;

    /**
     * @return ModelerSettingDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerSetting::class);
        return self::$instance;
    }

    public function findOneByCode($code) {
        return $this->criteria()->predicateAnd()->conditionEquals(ModelerAttributeConst::CODE, $code)->findOne();
    }

    public function getValueByCode() {

    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerSetting[]
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->conditionContains(ModelerAttributeConst::CODE, $findValue)
            ->findAll($start, $limit);
    }

}