<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEnumValueAttributeDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerEnumValueAttributeDAO
     */
    private static $instance = null;

    /**
     * @return ModelerEnumValueAttributeDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerEnumAttribute::class);
        return self::$instance;
    }

    public function findByEnumValueAttribute($enumId, $enumValueId, $attributeId) {

    }

}