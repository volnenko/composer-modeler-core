<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEntityDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerEntityDAO
     */
    private static $instance = null;

    /**
     * @return ModelerEntityDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerEntity::class);
        return self::$instance;
    }

    /**
     * @param $code
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findByCode($code) {
        return $this->criteria()->predicateAnd()->conditionEquals(ModelerAttributeConst::CODE, $code)->findOne();
    }

    public function getCodeById($entityId) {
        if (empty($entityId)) return null;
        $entity = $this->findOne($entityId);
        if ($entity === null) return null;
        return $entity->code;
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
                ->conditionContains(ModelerAttributeConst::NAME, $findValue)
                ->conditionContains(ModelerAttributeConst::CODE, $findValue)
            ->findAll($start, $limit);
    }

}