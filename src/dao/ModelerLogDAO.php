<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLogDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerLogDAO
     */
    private static $instance = null;

    /**
     * @return ModelerLogDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerLog::class);
        return self::$instance;
    }

    /**
     * @param $logTypeId
     * @param $message
     * @throws ModelerAbstractEntityException
     */
    private function addLog($logTypeId, $message)
    {
        $log = new ModelerLog();
        $log->setLogTypeId($logTypeId);
        $log->setMessage($message);
        $this->em->persist($log);
    }

    /**
     * @param string $message
     * @throws ModelerAbstractEntityException
     */
    public function info($message)
    {
        $this->addLog(ModelerLogType::INFO, $message);
    }

    /**
     * @param string $message
     * @throws ModelerAbstractEntityException
     */
    public function warn($message) {
        $this->addLog(ModelerLogType::WARN, $message);
    }

    /**
     * @param string $message
     * @throws ModelerAbstractEntityException
     */
    public function trace($message) {
        $this->addLog(ModelerLogType::TRACE, $message);
    }

    /**
     * @param string $message
     * @throws ModelerAbstractEntityException
     */
    public function error($message) {
        $this->addLog(ModelerLogType::ERROR, $message);
    }

}