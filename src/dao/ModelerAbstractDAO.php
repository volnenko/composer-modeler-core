<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

abstract class ModelerAbstractDAO implements ModelerDataAccessAPI
{

    /**
     * @var ModelerEntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var ModelerContentDAO
     */
    protected $contentDAO;

    /**
     * ModelerAbstractDAO constructor.
     * @param $entityClass
     */
    public function __construct($entityClass)
    {
        $this->em = ModelerEntityManager::getInstance();
        $this->entityClass = $entityClass;
        $this->contentDAO = new ModelerContentDAO($entityClass);
    }

    /**
     * @return bool
     */
    function isEmpty()
    {
        return $this->em->isEmpty($this->entityClass);
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->em->count($this->entityClass);
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists($id)
    {
        return $this->em->exists($this->entityClass, $id);
    }

    /**
     * @return array
     */
    public function ids()
    {
        return $this->em->ids($this->entityClass);
    }

    /**
     * @param string $id
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findOne($id)
    {
        return $this->em->findOne($this->entityClass, $id);
    }

    /**
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAll($start = null, $limit = null, $sortAttribute = null, $sortTypeId = null)
    {
        return $this->em->findAll($this->entityClass, $start, $limit, $sortAttribute, $sortTypeId);
    }

    /**
     * @param string $id
     */
    public function removeById($id)
    {
        $this->em->removeById($this->entityClass, $id);
    }

    /**
     * @return void
     */
    public function removeAll()
    {
        $this->em->removeAll($this->entityClass);
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function findOneByAttribute($attribute, $value)
    {
        return $this->em->findOneByAttribute($this->entityClass, $attribute, $value);
    }

    /**
     * @param string $findAttribute
     * @param mixed $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAllByAttribute($findAttribute, $findValue, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null)
    {
        return $this->em->findAllByAttribute($this->entityClass, $findAttribute, $findValue, $start, $limit, $sortAttribute, $sortTypeId);
    }

    /**
     * @param string $findAttribute
     * @param mixed $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function likeAllByAttribute($findAttribute, $findValue, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null)
    {
        return $this->em->likeAllByAttribute($this->entityClass, $findAttribute, $findValue, $start, $limit, $sortTypeId, $sortAttribute);
    }

    /**
     * @return ModelerContentAPI
     */
    public function content()
    {
        return $this->contentDAO;
    }


    /**
     * @return ModelerCriteria
     */
    public function criteria()
    {
        return ModelerCriteria::create($this->entityClass);
    }

    /**
     * @param ModelerAbstractEntity|null $entity
     * @throws ModelerEntityClassIncorrectException
     */
    private function checkEntityClass($entity)
    {
        if ($entity === null) return;
        if (get_class($entity) !== $this->entityClass) throw new ModelerEntityClassIncorrectException();
    }

    /**
     * @param ModelerAbstractEntity|null $entity
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     * @throws ModelerEntityClassIncorrectException
     */
    public function persist(ModelerAbstractEntity $entity)
    {
        $this->checkEntityClass($entity);
        return ModelerEntityManager::getInstance()->persist($entity);
    }

    /**
     * @param ModelerAbstractEntity|null $entity
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     * @throws ModelerEntityClassIncorrectException
     */
    public function merge(ModelerAbstractEntity $entity)
    {
        $this->checkEntityClass($entity);
        return ModelerEntityManager::getInstance()->merge($entity);
    }

    /**
     * @param ModelerAbstractEntity|null $entity
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     * @throws ModelerEntityClassIncorrectException
     */
    public function remove(ModelerAbstractEntity $entity)
    {
        $this->checkEntityClass($entity);
        return ModelerEntityManager::getInstance()->remove($entity);
    }

    /**
     * @param ModelerAbstractEntity|null $entity
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     * @throws ModelerEntityClassIncorrectException
     */
    public function refresh(ModelerAbstractEntity $entity)
    {
        $this->checkEntityClass($entity);
        return ModelerEntityManager::getInstance()->refresh($entity);
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return array|ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function removeAllByAttribute($attribute, $value)
    {
        $entity = $this->findOneByAttribute($attribute, $value);
        if ($entity === null) return null;
        return $this->remove($entity);
    }

}