<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEntityAttributeDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerEntityAttributeDAO
     */
    private static $instance = null;

    /**
     * @return ModelerEntityAttributeDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerEntityAttribute::class);
        return self::$instance;
    }

    /**
     * @param $entityId
     * @param $attributeId
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findOneByEntityAndAttribute($entityId, $attributeId) {
        return $this->criteria()->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::ENTITY_ID, $entityId)
            ->conditionEquals(ModelerAttributeConst::ATTRIBUTE_ID, $attributeId)
            ->findOne();
    }

    /**
     * @param string $entityId
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerEntityAttribute[]
     * @throws ModelerAbstractEntityException
     */
    public function findAllByEntity($entityId, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::ENTITY_ID, $entityId)
            ->findAll($start, $limit);
    }

    /**
     * @param string $entityId
     * @return ModelerEntityAttribute[]
     * @throws ModelerAbstractEntityException
     */
    public function findAllByEntitySupportDisplayField($entityId) {
        return $this->criteria()
            ->sort(ModelerAttributeConst::ORDER_INDEX, ModelerSortType::ASC)->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::ENTITY_ID, $entityId)
            ->conditionEquals(ModelerAttributeConst::SUPPORT_DISPLAY_NAME, true)
            ->findAll();

    }

    /**
     * @param string $entityId
     * @return ModelerEntityAttribute[]
     * @throws ModelerAbstractEntityException
     */
    public function findAllByEntitySupportSearchField($entityId) {
        return $this->criteria()
            ->sort(ModelerAttributeConst::ORDER_INDEX, ModelerSortType::ASC)->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::ENTITY_ID, $entityId)
            ->conditionEquals(ModelerAttributeConst::SUPPORT_SEARCH_FIELD, true)
            ->findAll();

    }

}