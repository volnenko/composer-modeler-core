<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerRoleDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerRoleDAO
     */
    private static $instance = null;

    /**
     * @return ModelerRoleDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerRole::class);
        return self::$instance;
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerRole[]
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->findAll($start, $limit);
    }

    /**
     * @param $name
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function findOneByName($name) {
        if (empty($name)) return null;
        return $this->criteria()
            ->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::NAME, $name)
            ->setLimit(1)
            ->findOne();
    }

    /**
     * @param $code
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function findOneByCode($code) {
        if (empty($code)) return null;
        return $this->criteria()
            ->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::CODE, $code)
            ->setLimit(1)
            ->findOne();
    }

    /**
     * @param $name
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function mergeByName($name) {
        $role = $this->findOneByName($name);
        if ($role === null) return null;
        $role = new ModelerRole();
        $role->name = $name;
        return $role->merge();
    }

    /**
     * @param ModelerRoleType $userType
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function findByUserType($userType) {
        $name = $userType->getName();
        $code = $userType->getCode();
        if (empty($name) || empty($code)) return null;
        return $this->criteria()
            ->setLimit(1)
            ->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::NAME, $name)
            ->conditionEquals(ModelerAttributeConst::CODE, $code)
            ->findOne();
    }

    /**
     * @param $userTypes
     * @return ModelerRole[]
     * @throws ModelerAbstractEntityException
     */
    public function mergeUserTypes($userTypes) {
        $result = array();
        foreach ($userTypes as $userType) {
            $result[] = $this->mergeUserType($userType);
        }
        return $result;
    }

    /**
     * @param ModelerRoleType $userType
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function mergeUserType($userType) {
        if ($userType === null) return null;
        $role = $this->findByUserType($userType);
        if ($role !== null) return $role;
        $role = new ModelerRole();
        $role->setName($userType->getName());
        $role->setCode($userType->getCode());
        return $role->merge();
    }

}