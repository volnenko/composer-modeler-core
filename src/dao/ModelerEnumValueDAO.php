<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEnumValueDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerEnumValueDAO
     */
    private static $instance = null;

    /**
     * @return ModelerEnumValueDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerEnumValue::class);
        return self::$instance;
    }

    /**
     * @param string $code
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findByCode($code) {
        return $this->criteria()->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::CODE, $code)->findOne();
    }

    /**
     * @param string $enumId
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAllByEnumId($enumId, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateAnd()
                ->conditionEquals(ModelerAttributeConst::ENUM_ID, $enumId)
            ->findAll($start, $limit);
    }

    /**
     * @param string $findValue
     * @param string $enumId
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function likeAllByEnumId($findValue, $enumId, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateAnd()
            ->conditionEquals(ModelerAttributeConst::ENUM_ID, $enumId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->conditionContains(ModelerAttributeConst::CODE, $findValue)
            ->findAll($start, $limit);
    }

    /**
     * @param string $enumId
     * @return int
     * @throws ModelerAbstractEntityException
     */
    public function countByEnumId($enumId) {
        return $this->criteria()->predicateAnd()->conditionEquals(ModelerAttributeConst::ENUM_ID, $enumId)->count();
    }

}