<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerContentDAO implements ModelerContentAPI
{
    /**
     * @var string
     */
    protected $entityClass;

    /**
     * ModelerContentDAO constructor.
     * @param string $entityClass
     */
    public function __construct($entityClass)
    {
        $this->entityClass = $entityClass;
    }

    /**
     * @param string $id
     * @return bool|null|string
     */
    public function get($id)
    {
        return ModelerStorageManager::getInstance()->getEntityContent($this->entityClass, $id);
    }

    /**
     * @param string $id
     * @param mixed $content
     */
    public function set($id, $content)
    {
        ModelerStorageManager::getInstance()->setEntityContent($this->entityClass, $id, $content);
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists($id)
    {
        return ModelerStorageManager::getInstance()->hasEntityContent($this->entityClass, $id);
    }


    /**
     * @param string $id
     */
    public function remove($id)
    {
        ModelerStorageManager::getInstance()->removeById($this->entityClass, $id);
    }

}