<?php namespace Volnenko\Modeler\Dao;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerTagDAO extends ModelerAbstractDAO
{

    /**
     * @var ModelerTagDAO
     */
    private static $instance = null;

    /**
     * @return ModelerTagDAO
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerTag::class);
        return self::$instance;
    }

    /**
     * @param string $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerTag[]
     * @throws ModelerAbstractEntityException
     */
    public function likeByFindValue(
        $findValue, $start = null, $limit = null,
        $sortAttribute = ModelerAttributeConst::ORDER_INDEX, $sortTypeId = ModelerSortType::DESC
    ) {
        return $this->criteria()
            ->sort($sortAttribute, $sortTypeId)
            ->predicateOr()
            ->conditionContains(ModelerAttributeConst::NAME, $findValue)
            ->findAll($start, $limit);
    }


}