<?php namespace Volnenko\Modeler\Gui;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerBuilderWEB
{

    /**
     * @var string
     */
    private static $title = 'MODELER';

    /**
     * @var string
     */
    private static $header = '';

    /**
     * @var string
     */
    private static $description = '';

    /**
     * @param $title
     * @return string
     */
    public static function addTitle($title) {
        self::$title .= ' | ';
        self::$title .= $title;
        return self::$title;
    }

    /**
     * @return string
     */
    public static function getTitle()
    {
        return self::$title;
    }

    /**
     * @param string $title
     */
    public static function setTitle($title)
    {
        self::$title = $title;
    }

    /**
     * @return string
     */
    public static function getDescription()
    {
        return self::$description;
    }

    /**
     * @param string $description
     */
    public static function setDescription($description)
    {
        self::$description = $description;
    }

    /**
     * @return string
     */
    public static function getHeader()
    {
        return self::$header;
    }

    /**
     * @param string $header
     */
    public static function setHeader($header)
    {
        self::$header = $header;
    }

}