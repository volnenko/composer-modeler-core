<?php namespace Volnenko\Modeler\Gui;

use Volnenko\Modeler\Constant\ModelerAttributeConst;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerBuilderURL
{

    private $params = array();

    private $document = '';

    /**
     * ModelerBuilderURL constructor.
     * @param string $document
     */
    public function __construct($document = '')
    {
        $this->document = $document;
    }

    /**
     * @param $key
     * @param $value
     * @return ModelerBuilderURL
     */
    private function put($key, $value) {
        if (empty($key) || empty($value)) return $this;
        $this->params[$key] = $value;
        return $this;
    }

    /**
     * @param string $document
     * @return ModelerBuilderURL
     */
    public static function create($document = '') {
        return new ModelerBuilderURL($document);
    }

    /**
     * @param string $id
     * @return ModelerBuilderURL
     */
    public function id($id) {
        return $this->put(ModelerAttributeConst::ID, $id);
    }

    /**
     * @param string $userId
     * @return ModelerBuilderURL
     */
    public function userId($userId) {
        return $this->put(ModelerAttributeConst::USER_ID, $userId);
    }

    /**
     * @param string $operation
     * @return ModelerBuilderURL
     */
    public function operation($operation) {
        return $this->put(ModelerAttributeConst::OPERATION, $operation);
    }

    /**
     * @param string $id
     * @return ModelerBuilderURL
     */
    public function settingId($id) {
        return $this->put(ModelerAttributeConst::SETTING_ID, $id);
    }

    /**
     * @param int $start
     * @return ModelerBuilderURL
     */
    public function start($start) {
        return $this->put(ModelerAttributeConst::START, $start);
    }

    /**
     * @param int $limit
     * @return ModelerBuilderURL
     */
    public function limit($limit) {
        return $this->put(ModelerAttributeConst::LIMIT, $limit);
    }

    /**
     * @param string $enumId
     * @return ModelerBuilderURL
     */
    public function enumId($enumId) {
        return $this->put(ModelerAttributeConst::ENUM_ID, $enumId);
    }

    /**
     * @param string $entityId
     * @return ModelerBuilderURL
     */
    public function entityId($entityId) {
        return $this->put(ModelerAttributeConst::ENTITY_ID, $entityId);
    }

    /**
     * @param string $localeId
     * @return ModelerBuilderURL
     */
    public function localeId($localeId) {
        return $this->put(ModelerAttributeConst::LOCALE_ID, $localeId);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function time() {
        return $this->put(ModelerAttributeConst::TIME, ModelerDateTime::timestamp());
    }

    /**
     * @param string $findValue
     * @return ModelerBuilderURL
     */
    public function findValue($findValue) {
        return $this->put(ModelerAttributeConst::FIND_VALUE, $findValue);
    }

    /**
     * @param string $xtype
     * @return ModelerBuilderURL
     */
    public function xtype($xtype) {
        return $this->put(ModelerAttributeConst::XTYPE, $xtype);
    }

    /**
     * @param string $folderId
     * @return ModelerBuilderURL
     */
    public function folderId($folderId) {
        return $this->put(ModelerAttributeConst::FOLDER_ID, $folderId);
    }

    /**
     * @param string $sortField
     * @return ModelerBuilderURL
     */
    public function sortField($sortField) {
        return $this->put(ModelerAttributeConst::SORT_FIELD, $sortField);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldOrderIndex() {
        return $this->sortField(ModelerAttributeConst::ORDER_INDEX);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldIP() {
        return $this->sortField(ModelerAttributeConst::IP);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldHost() {
        return $this->sortField(ModelerAttributeConst::HOST);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldName() {
        return $this->sortField(ModelerAttributeConst::NAME);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldValue() {
        return $this->sortField(ModelerAttributeConst::VALUE);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldDescription() {
        return $this->sortField(ModelerAttributeConst::DESCRIPTION);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldSize() {
        return $this->sortField(ModelerAttributeConst::SIZE);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldId() {
        return $this->sortField(ModelerAttributeConst::ID);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldAttributeTypeId() {
        return $this->sortField(ModelerAttributeConst::ATTRIBUTE_TYPE_ID);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldCode() {
        return $this->sortField(ModelerAttributeConst::CODE);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldEmail() {
        return $this->sortField(ModelerAttributeConst::EMAIL);
    }

    /**
     * @return ModelerBuilderURL
     */
    public function sortFieldLogin() {
        return $this->sortField(ModelerAttributeConst::LOGIN);
    }

    /**
     * @param $sortType
     * @return ModelerBuilderURL
     */
    public function sortType($sortType) {
        return $this->put(ModelerAttributeConst::SORT_TYPE, $sortType);
    }

    /**
     * @param string $document
     * @return ModelerBuilderURL
     */
    public function document($document) {
        $this->document = $document;
        return $this;
    }

    /**
     * @return string
     */
    public function toURL() {
        if (empty($this->params)) return $this->document;
        return $this->document . '?' . http_build_query($this->params);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toURL();
    }

}