<?php namespace Volnenko\Modeler\Dto;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerErrorDTO extends ModelerResultDTO
{

    /**
     * ModelerErrorDTO constructor.
     * @param string $message
     */
    public function __construct($message = '')
    {
        parent::__construct(false, $message);
    }

}