<?php namespace Volnenko\Modeler\Dto;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSuccessDTO extends ModelerResultDTO
{

    /**
     * ModelerSuccessDTO constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setSuccess(true);
    }

}