<?php namespace Volnenko\Modeler\Dto;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerResultDTO extends ModelerAbstractDTO
{

    /**
     * @var bool
     */
    var $success;

    /**
     * @var string
     */
    var $message;

    /**
     * ModelerResultDTO constructor.
     * @param bool $success
     * @param string $message
     */
    public function __construct($success = true, $message = '')
    {
        $this->success = $success;
        $this->message = $message;
    }


    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

}