<?php namespace Volnenko\Modeler\Dto;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerCountDTO extends ModelerAbstractDTO
{

    /**
     * @var int
     */
    var $count;

    /**
     * ModelerCountDTO constructor.
     * @param int $count
     */
    public function __construct($count = 0)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

}