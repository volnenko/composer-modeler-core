<?php namespace Volnenko\Modeler\Dto;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEntityInfoDTO extends ModelerAbstractDTO
{

    /**
     * @var string
     */
    var $entityName;

    /**
     * @var string
     */
    var $entityId;

    /**
     * @var string
     */
    var $entityFolder;

    /**
     * @var string
     */
    var $recordFolder;

    /**
     * @var string
     */
    var $recordFile;

    /**
     * @var string
     */
    var $metaFile;

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @param string $entityName
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string
     */
    public function getRecordFolder()
    {
        return $this->recordFolder;
    }

    /**
     * @param string $recordFolder
     */
    public function setRecordFolder($recordFolder)
    {
        $this->recordFolder = $recordFolder;
    }

    /**
     * @return string
     */
    public function getRecordFile()
    {
        return $this->recordFile;
    }

    /**
     * @param string $recordFile
     */
    public function setRecordFile($recordFile)
    {
        $this->recordFile = $recordFile;
    }

    /**
     * @return string
     */
    public function getMetaFile()
    {
        return $this->metaFile;
    }

    /**
     * @param string $metaFile
     */
    public function setMetaFile($metaFile)
    {
        $this->metaFile = $metaFile;
    }

    /**
     * @return string
     */
    public function getEntityFolder()
    {
        return $this->entityFolder;
    }

    /**
     * @param string $entityFolder
     */
    public function setEntityFolder($entityFolder)
    {
        $this->entityFolder = $entityFolder;
    }

}