<?php namespace Volnenko\Modeler\Manager;

use Volnenko\Modeler\Constant\ModelerAttributeConst;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerImageManager
{

    /**
     * @var ModelerImageManager
     */
    private static $instance = null;

    /**
     * @return ModelerImageManager
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @param $upload
     * @param null $folderId
     * @return ModelerImage|null
     * @throws ModelerAbstractEntityException
     */
    public function uploadImage($upload, $folderId = null) {
        if (empty($upload[ModelerAttributeConst::NAME])) return null;
        if (empty($upload[ModelerAttributeConst::TYPE])) return null;
        if (empty($upload[ModelerAttributeConst::SIZE])) return null;
        if (empty($upload[ModelerAttributeConst::TMP_NAME])) return null;
        $image = new ModelerImage();
        $image->name = $upload[ModelerAttributeConst::NAME];
        $image->fileName = $upload[ModelerAttributeConst::NAME];
        $image->extension = pathinfo($upload[ModelerAttributeConst::NAME], PATHINFO_EXTENSION);
        $image->contentType = $upload[ModelerAttributeConst::TYPE];
        $image->size = $upload[ModelerAttributeConst::SIZE];
        if (!empty($folderId)) $image->folderId = $folderId;
        $image->persist();
        $content = file_get_contents($upload[ModelerAttributeConst::TMP_NAME]);
        $image->content()->set($content);
        return $image;
    }

    /**
     * @param string $imageId
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function getImageById($imageId) {
        return ModelerImageDAO::getInstance()->findOne($imageId);
    }

    /**
     * @param string $imageId
     * @return null|mixed
     * @throws ModelerAbstractEntityException
     */
    public function getImageContentById($imageId) {
        $image = $this->getImageById($imageId);
        if ($image === null) return null;
        return $image->content()->get();
    }

    /**
     * @param string $imageId
     * @return bool
     * @throws ModelerAbstractEntityException
     */
    public function hasImageContentById($imageId) {
        $image = $this->getImageById($imageId);
        if ($image === null) return false;
        return $image->content()->exists();
    }

    /**
     * @param string $imageId
     * @throws ModelerAbstractEntityException
     */
    public function removeImageContentById($imageId) {
        $image = $this->getImageById($imageId);
        if ($image === null) return;
        $image->content()->remove();
    }

    /**
     * @param string $imageId
     */
    public function removeImageById($imageId) {
        ModelerImageDAO::getInstance()->removeById($imageId);
    }

}