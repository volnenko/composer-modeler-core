<?php namespace Volnenko\Modeler\Manager;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLocaleManager
{

    /**
     * @var ModelerLocaleManager
     */
    private static $instance = null;

    /**
     * @return ModelerLocaleManager
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function addLocale($code, $name)
    {
        $locale = new ModelerLocale();
        $locale->setName($name);
        $locale->setCode($code);
        return $locale->persist();
    }

    /**
     * @param string $localeCode
     * @param string $localeKeyName
     */
    public function addLocaleKey($localeCode, $localeKeyName)
    {
        //TODO Implement addLocaleKey
    }

    /**
     * @param string $localeCode
     * @param string $localeKeyName
     * @param string $localeKeyValue
     */
    public function addLocaleKeyValue($localeCode, $localeKeyName, $localeKeyValue)
    {
        //TODO Implement addLocaleKeyValue
    }

}