<?php namespace Volnenko\Modeler\Manager;

use Volnenko\Modeler\Constant\ModelerAttributeConst;
use Volnenko\Modeler\Dao\ModelerFileDAO;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerFileManger
{

    /**
     * @var ModelerFileManger
     */
    private static $instance = null;

    /**
     * @return ModelerFileManger
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @param $upload
     * @param null $folderId
     * @return ModelerFile|null
     * @throws ModelerAbstractEntityException
     */
    public function uploadFile($upload, $folderId = null) {
        if (empty($upload[ModelerAttributeConst::NAME])) return null;
        if (empty($upload[ModelerAttributeConst::TYPE])) return null;
        if (empty($upload[ModelerAttributeConst::SIZE])) return null;
        if (empty($upload[ModelerAttributeConst::TMP_NAME])) return null;
        $file = new ModelerFile();
        $file->name = $upload[ModelerAttributeConst::NAME];
        $file->fileName = $upload[ModelerAttributeConst::NAME];
        $file->extension = pathinfo($upload[ModelerAttributeConst::NAME], PATHINFO_EXTENSION);
        $file->contentType = $upload[ModelerAttributeConst::TYPE];
        $file->size = $upload[ModelerAttributeConst::SIZE];
        if (!empty($folderId)) $file->folderId = $folderId;
        $file->persist();
        $content = file_get_contents($upload[ModelerAttributeConst::TMP_NAME]);
        $file->content()->set($content);
        return $file;
    }

    /**
     * @param string $fileId
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function getFileById($fileId) {
        return ModelerFileDAO::getInstance()->findOne($fileId);
    }

    /**
     * @param string $fileId
     * @return null|mixed
     * @throws ModelerAbstractEntityException
     */
    public function getFileContentById($fileId) {
        $file = $this->getFileById($fileId);
        if ($file === null) return null;
        return $file->content()->get();
    }

    /**
     * @param string $fileId
     * @return bool
     * @throws ModelerAbstractEntityException
     */
    public function hasFileContentById($fileId) {
        $file = $this->getFileById($fileId);
        if ($file === null) return false;
        return $file->content()->exists();
    }

    /**
     * @param string $fileId
     * @throws ModelerAbstractEntityException
     */
    public function removeFileContentById($fileId) {
        $file = $this->getFileById($fileId);
        if ($file === null) return;
        $file->content()->remove();
    }

    /**
     * @param string $fileId
     */
    public function removeFileById($fileId) {
        ModelerFileDAO::getInstance()->removeById($fileId);
    }

}