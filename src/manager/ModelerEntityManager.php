<?php namespace Volnenko\Modeler\Manager;

use Volnenko\Modeler\Dao\ModelerEntityDAO;
use Volnenko\Modeler\Criteria\ModelerCriteria;
use Volnenko\Modeler\Manager\ModelerStorageManager;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEntityManager
{
    private static $instance = null;

    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @param string $entityClass
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAll($entityClass, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null) {
        $sortSupport = !empty($sortAttribute) && !empty($sortTypeId);
        if ($sortSupport) return ModelerCriteria::create($entityClass)
            ->sort($sortAttribute, $sortTypeId)
            ->findAll($start, $limit);
        return ModelerStorageManager::getInstance()->findAll($entityClass, $start, $limit);
    }

    /**
     * @param string $entityClass
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAllRaw($entityClass, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null) {
        $sortSupport = !empty($sortAttribute) && !empty($sortTypeId);
        if ($sortSupport) return ModelerCriteria::create($entityClass)
            ->setRaw(true)
            ->sort($sortAttribute, $sortTypeId)
            ->findAll($start, $limit);
        return ModelerStorageManager::getInstance()->findAllRaw($entityClass, $start, $limit);
    }

    /**
     * @param string $entityClass
     * @param string $recordId
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function findOne($entityClass, $recordId)
    {
        return ModelerStorageManager::getInstance()->findOne($entityClass, $recordId);
    }

    /**
     * @param string $entityClass
     * @param string $recordId
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function findOneRaw($entityClass, $recordId)
    {
        return ModelerStorageManager::getInstance()->findOneRaw($entityClass, $recordId);
    }

    /**
     * @param string $entityClass
     * @param string $recordId
     * @return bool
     */
    public function exists($entityClass, $recordId)
    {
        return ModelerStorageManager::getInstance()->exists($entityClass, $recordId);
    }

    /**
     * @param string $entityClass
     * @return int
     */
    public function count($entityClass) {
        if (empty($entityClass)) return 0;
        return count($this->ids($entityClass));
    }

    /**
     * @param string $entityClass
     * @return bool
     */
    public function isEmpty($entityClass) {
        return $this->count($entityClass) === 0;
    }

    /**
     * @param string $entityClass
     * @param string $attribute
     * @param mixed $value
     * @return mixed|null
     * @throws ModelerAbstractEntityException
     */
    public function findOneByAttribute($entityClass, $attribute, $value)
    {
        return ModelerCriteria::create($entityClass)->predicateAnd()->conditionEquals($attribute, $value)->findOne();
    }

    /**
     * @param ModelerCriteria $criteria
     * @return array
     */
    public function findByCriteria(ModelerCriteria $criteria) {
        return array();
    }

    /**
     * @param string $entityClass
     * @param string $findAttribute
     * @param mixed $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAllByAttribute($entityClass, $findAttribute, $findValue, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null) {
        return ModelerCriteria::create($entityClass)
            ->sort($sortAttribute, $sortTypeId)
            ->predicateAnd()->conditionEquals($findAttribute, $findValue)
            ->findAll($start, $limit);
    }

    /**
     * @param string $entityClass
     * @param string $findAttribute
     * @param int|string|double|float|DateTime|bool|boolean $findValue
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function likeAllByAttribute($entityClass, $findAttribute, $findValue, $start = null, $limit = null, $sortAttribute = null, $sortTypeId = null) {
        return ModelerCriteria::create($entityClass)
            ->sort($sortAttribute, $sortTypeId)
            ->predicateAnd()->conditionContains($findAttribute, $findValue)
            ->findAll($start, $limit);
    }

    /**
     * @param ModelerAbstractEntity $record
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function persist(ModelerAbstractEntity $record)
    {
        return ModelerStorageManager::getInstance()->persist($record);
    }

    /**
     * @param string $entityClass
     * @param stdClass $record
     * @throws ModelerAbstractEntityException
     */
    public function persistRaw($entityClass, $record) {
        ModelerStorageManager::getInstance()->mergeRaw($entityClass, $record);
    }

    /**
     * @param string $entityClass
     * @param stdClass $record
     * @throws ModelerAbstractEntityException
     */
    public function mergeRaw($entityClass, $record) {
        ModelerStorageManager::getInstance()->mergeRaw($entityClass, $record);
    }

    /**
     * @param ModelerAbstractEntity $record
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function merge(ModelerAbstractEntity $record)
    {
        return ModelerStorageManager::getInstance()->merge($record);
    }

    /**
     * @param $record
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function refresh(ModelerAbstractEntity $record)
    {
        return ModelerStorageManager::getInstance()->refresh($record);
    }

    /**
     * @param ModelerAbstractEntity $record
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function remove(ModelerAbstractEntity $record)
    {
        return ModelerStorageManager::getInstance()->remove($record);
    }

    /**
     * @param string $entityClass
     * @return array
     */
    public function ids($entityClass) {
        return ModelerStorageManager::getInstance()->getEntityIds($entityClass);
    }

    /**
     * @param $entityClass
     * @param $recordIdSource
     * @param $recordIdTarget
     * @throws ModelerAbstractEntityException
     */
    public function reorder($entityClass, $recordIdSource, $recordIdTarget) {
        $source = ModelerStorageManager::getInstance()->findOne($entityClass, $recordIdSource);
        if ($source == null) return;
        $target = ModelerStorageManager::getInstance()->findOne($entityClass, $recordIdTarget);
        if ($target == null) return;
        $orderIndexSource = $source->orderIndex;
        $orderIndexTarget = $target->orderIndex;
        $source->orderIndex = $orderIndexTarget;
        $target->orderIndex = $orderIndexSource;
        ModelerStorageManager::getInstance()->merge($source);
        ModelerStorageManager::getInstance()->merge($target);
    }

    /**
     * @param string $entityClass
     * @param string $recordId
     */
    public function removeById($entityClass, $recordId)
    {
        ModelerStorageManager::getInstance()->removeById($entityClass, $recordId);
    }

    /**
     * @param string $entityClass
     * @return void
     */
    public function removeAll($entityClass)
    {
        ModelerStorageManager::getInstance()->removeAll($entityClass);
    }

    /**
     * @param $entityId
     * @param $recordId
     * @return string
     * @throws ModelerAbstractEntityException
     */
    public function getDisplayNameByEntityRecordId($entityId, $recordId) {
        $entityAttributes = ModelerEntityAttributeDAO::getInstance()->findAllByEntitySupportDisplayField($entityId);
        $fields = array();
        foreach ($entityAttributes as $entityAttribute) {
            $attributeId = $entityAttribute->attributeId;
            if (empty($attributeId)) continue;
            $attribute = ModelerAttributeDAO::getInstance()->findOne($attributeId);
            if ($attribute === null) continue;
            $attributeCode = $attribute->code;
            if (empty($attributeCode)) continue;
            $fields[] = $attributeCode;
        }
        $entityClass = ModelerEntityDAO::getInstance()->getCodeById($entityId);
        $record = ModelerStorageManager::getInstance()->findOneRaw($entityClass, $recordId);
        if (empty($record)) return '';
        if (empty($fields)) return $record->id;
        $result = '';
        foreach ($record as $name => $value) {
            $check = in_array($name, $fields);
            if ($check) $result .= $value;
        }
        return $result;
    }

}