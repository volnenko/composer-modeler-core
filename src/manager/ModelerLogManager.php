<?php namespace Volnenko\Modeler\Manager;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLogManager
{

    /**
     * @var ModelerLogManager
     */
    private static $instance = null;

    /**
     * @return ModelerLogManager
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @param $message
     * @throws ModelerAbstractEntityException
     */
    public function info($message) {
        return ModelerLogDAO::getInstance()->info($message);
    }

    /**
     * @param $message
     * @throws ModelerAbstractEntityException
     */
    public function warn($message) {
        return ModelerLogDAO::getInstance()->warn($message);
    }

    /**
     * @param $message
     * @throws ModelerAbstractEntityException
     */
    public function trace($message) {
        return ModelerLogDAO::getInstance()->trace($message);
    }

    /**
     * @param $message
     * @throws ModelerAbstractEntityException
     */
    public function error($message) {
        return ModelerLogDAO::getInstance()->error($message);
    }

}