<?php namespace Volnenko\Modeler\Manager;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerUserManager
{

    /**
     * @var ModelerUserManager
     */
    private static $instance = null;

    /**
     * @return ModelerUserManager
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @param $userId
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function getUserById($userId) {
        return ModelerUserDAO::getInstance()->getUserById($userId);
    }

    /**
     * @param $login
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function getUserByLogin($login) {
        return ModelerUserDAO::getInstance()->getUserByLogin($login);
    }

    /**
     * @param $email
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function getUserByEmail($email) {
        return ModelerUserDAO::getInstance()->getUserByEmail($email);
    }

    /**
     * @param $userId
     * @return ModelerAbstractEntity|null
     */
    public function removeUserById($userId) {
        return ModelerUserDAO::getInstance()->removeUserById($userId);
    }

    /**
     * @param $login
     * @throws ModelerAbstractEntityException
     */
    public function removeUserByLogin($login) {
        return ModelerUserDAO::getInstance()->removeUserByLogin($login);
    }

    /**
     * @param $email
     * @throws ModelerAbstractEntityException
     */
    public function removeUserByEmail($email) {
        return ModelerUserDAO::getInstance()->removeUserByEmail($email);
    }

    /**
     * @param $userId
     * @return ModelerUser|null
     * @throws ModelerAbstractEntityException
     */
    public function lockUserById($userId) {
        return ModelerUserDAO::getInstance()->lockUserById($userId);
    }

    /**
     * @param $userId
     * @return null|ModelerUser
     * @throws ModelerAbstractEntityException
     */
    public function unlockUserById($userId) {
        return ModelerUserDAO::getInstance()->unlockUserById($userId);
    }

    /**
     * @param string $login
     * @param string $password
     * @param string $email
     * @param string $roleTypeId
     * @throws ModelerAbstractEntityException
     * @throws ModelerEmailEmptyException
     * @throws ModelerLoginEmptyException
     * @throws ModelerPasswordEmptyException
     * @throws ModelerUserExistsException
     */
    public function createUser($login, $password, $email = null, $roleTypeId = null) {
        $unique = ModelerUserDAO::getInstance()->isLoginUnique($login);
        if (!$unique) throw new ModelerUserExistsException();
        if (empty($password)) throw new ModelerPasswordEmptyException();
        $user = ModelerUserDAO::getInstance()->createUser($login, $password, $email);
        $role = ModelerRoleDAO::getInstance()->findOneByCode($roleTypeId);
        if ($role === null) return;
        ModelerUserRoleDAO::getInstance()->mergeUserIdRoleId($user->id, $role->id);
    }

}