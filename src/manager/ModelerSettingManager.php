<?php namespace Volnenko\Modeler\Manager;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSettingManager
{

    /**
     * @var ModelerSettingManager|null
     */
    private static $instance = null;

    /**
     * @return ModelerSettingManager|null
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @param string $settingId
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function getSettingById($settingId)
    {
        return ModelerSettingDAO::getInstance()->findOne($settingId);
    }

    /**
     * @param string $code
     * @return ModelerSetting|null
     */
    public function getSettingByCode($code)
    {
        return ModelerSettingDAO::getInstance()->findOneByCode($code);
    }

    /**
     * @param string $code
     * @return ModelerSetting|null
     */
    public function getSetting($code)
    {
        return $this->getSettingByCode($code);
    }

    /**
     * @param string $code
     * @return mixed|null
     */
    public function getSettingValue($code)
    {
        $setting = $this->getSetting($code);
        if ($setting === null) return null;
        return $setting->value;
    }

    /**
     * @param string $code
     * @return bool
     */
    public function hasSetting($code) {
        $setting = $this->getSetting($code);
        return $setting !== null;
    }

    /**
     * @param string $code
     * @param integer $value
     * @param string $name
     * @param string $settingTypeId
     */
    private function initSettingValue($code, $value, $name = '', $settingTypeId = null) {
        if ($this->hasSetting($code)) return;
        $setting = $this->setSettingValue($code, $value);
        if (!empty($name)) {
            $setting->setName($name);
            $setting->setSettingTypeId($settingTypeId);
            $setting->merge();
        }
    }

    /**
     * @param string $code
     * @param string $value
     * @param string $name
     */
    public function initSettingValueString($code, $value, $name = '') {
        $this->initSettingValue($code, $value, $name, ModelerSettingType::STRING);
    }

    /**
     * @param string $code
     * @param integer $value
     * @param string $name
     */
    public function initSettingValueInteger($code, $value, $name = '') {
        $this->initSettingValue($code, $value, $name, ModelerSettingType::INTEGER);
    }

    /**
     * @param string $code
     * @param float $value
     * @param string $name
     */
    public function initSettingValueFloat($code, $value, $name = '') {
        $this->initSettingValue($code, $value, $name, ModelerSettingType::FLOAT);
    }

    /**
     * @param string $code
     * @param double $value
     * @param string $name
     */
    public function initSettingValueDouble($code, $value, $name = '') {
        $this->initSettingValue($code, $value, $name, ModelerSettingType::DOUBLE);
    }

    /**
     * @param string $code
     * @param integer $value
     * @param string $name
     */
    public function initSettingValueLong($code, $value, $name = '') {
        $this->initSettingValue($code, $value, $name, ModelerSettingType::LONG);
    }

    /**
     * @param string $code
     * @param DateTime $value
     * @param string $name
     */
    public function initSettingValueDate($code, $value, $name = '') {
        $this->initSettingValue($code, $value, $name, ModelerSettingType::DATE);
    }

    /**
     * @param string $code
     * @param bool $value
     * @param string $name
     */
    public function initSettingValueBoolean($code, $value, $name = '') {
        $this->initSettingValue($code, $value, $name, ModelerSettingType::BOOLEAN);
    }

//    public function addSettingValue($code, $value, $) {
//
//    }

    public function setSettingValue($code, $value)
    {
        $setting = $this->getSetting($code);
        if ($setting === null) $setting = new ModelerSetting();
        $setting->code = $code;
        $setting->value = $value;
        $setting->settingValueTypeId = ModelerSettingValueType::SINGLE;
        return $setting->merge();
    }

    public function getSettingValues($code)
    {

    }

    public function getSettingValueInteger($code)
    {

    }

    public function setSettingValueInteger($code, $value)
    {

    }

    public function addSettingValueInteger($code, $value)
    {

    }

    public function getSettingValueLong($code)
    {

    }

    public function setSettingValueLong($code, $value)
    {

    }

    /**
     * @param string $code
     * @param int|null $value
     * @throws ModelerAbstractSettingException
     */
    public function addSettingValueLong($code, $value)
    {
        $setting = $this->getSetting($code);
        if ($setting === null) throw new ModelerSettingNotFoundException();
        $checkValueType = is_long($value);
        if (!$checkValueType) throw new ModelerSettingValueNotLongException();
    }

    public function getSettingValueDate($code)
    {

    }

    public function setSettingValueDate($code, $value)
    {

    }

    public function addSettingValueDate($code, $value)
    {

    }

    public function getSettingValueBoolean($code)
    {

    }

    public function setSettingValueBoolean($code, $value)
    {

    }

    public function addSettingValueBoolean($code, $value)
    {

    }

    public function getSettingValueFloat($code)
    {

    }

    public function setSettingValueFloat($code, $value)
    {

    }

    public function addSettingValueFloat($code, $value)
    {

    }

    public function getSettingValueDouble($code)
    {

    }

    public function setSettingValueDouble($code, $value)
    {

    }

    public function addSettingValueDouble($code, $value)
    {

    }

}