<?php namespace Volnenko\Modeler\Manager;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSchemeManager
{

    /**
     * @var ModelerSchemeManager
     */
    private static $instance = null;

    /**
     * @return ModelerSchemeManager
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function getListEntity()
    {
        return ModelerEntityDAO::getInstance()->findAll();
    }

    /**
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function getListAttribute()
    {
        return ModelerAttributeDAO::getInstance()->findAll();
    }

    /**
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function getListEnum()
    {
        return ModelerEnumDAO::getInstance()->findAll();
    }

    /**
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function getListEnumAttribute()
    {
        return ModelerEnumAttributeDAO::getInstance()->findAll();
    }

    /**
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function getListEnumValue()
    {
        return ModelerEnumValueDAO::getInstance()->findAll();
    }

    /**
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function getListEntityAttribute()
    {
        return ModelerEnumAttributeDAO::getInstance()->findAll();
    }

    /**
     * @param string $code
     * @param string $name
     * @param bool $supportSystem
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function mergeEntity($code, $name, $supportSystem = false) {
        if (empty($code) || empty($name)) return null;
        $entity = ModelerEntityDAO::getInstance()->findByCode($code);
        if ($entity === null) $entity = new ModelerEntity();
        $entity->setCode($code);
        $entity->setName($name);
        $entity->setSupportSystem($supportSystem);
        return $entity->merge();
    }

    /**
     * @param string $code
     * @param string $name
     * @param string $attributeTypeId
     * @param bool $supportSystem
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function mergeAttribute($code, $name, $attributeTypeId, $supportSystem = false) {
        if (empty($code) || empty($name) || empty($attributeTypeId)) return null;
        $attribute = ModelerAttributeDAO::getInstance()->findByCode($code);
        if ($attribute === null) $attribute = new ModelerAttribute();
        $attribute->setCode($code);
        $attribute->setName($name);
        $attribute->setAttributeTypeId($attributeTypeId);
        $attribute->setSupportSystem($supportSystem);
        return $attribute->merge();
    }

    /**
     * @param string $entityCode
     * @param string $attributeCode
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function mergeEntityAttribute($entityCode, $attributeCode) {
        if (empty($entityCode) || empty($attributeCode)) return null;
        $entity = ModelerEntityDAO::getInstance()->findByCode($entityCode);
        if ($entity === null) return null;
        $attribute = ModelerAttributeDAO::getInstance()->findByCode($attributeCode);
        if ($attribute === null) return null;
        $entityId = $entity->getId();
        $attributeId = $attribute->getId();
        $entityAttribute = ModelerEntityAttributeDAO::getInstance()->findOneByEntityAndAttribute($entityId, $attributeId);
        if ($entityAttribute === null) $entityAttribute = new ModelerEntityAttribute();
        $entityAttribute->setEntityId($entityId);
        $entityAttribute->setAttributeId($attributeId);
        return $entityAttribute->merge();
    }

    /**
     * @param string $code
     * @param string $name
     * @param bool $supportSystem
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function mergeEnum($code, $name, $supportSystem = false) {
        if (empty($code) || empty($name)) return null;
        $enum = ModelerEnumDAO::getInstance()->findByCode($code);
        if ($enum === null) $enum = new ModelerEnum();
        $enum->setCode($code);
        $enum->setName($name);
        $enum->setSupportSystem($supportSystem);
        return $enum->merge();
    }

    /**
     * @param $enumCode
     * @param $attributeCode
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function mergeEnumAttribute($enumCode, $attributeCode) {
        if (empty($enumCode) || empty($attributeCode)) return null;
        $enum = ModelerEnumDAO::getInstance()->findByCode($enumCode);
        if ($enum === null) return null;
        $attribute = ModelerAttributeDAO::getInstance()->findByCode($attributeCode);
        if ($attribute === null) return null;
        $enumId = $enum->getId();
        $attributeId = $attribute->getId();
        $enumAttribute = ModelerEnumAttributeDAO::getInstance()->findOneByEnumAttribute($enumId, $attributeId);
        if ($enumAttribute === null) $enumAttribute = new ModelerEnumAttribute();
        $enumAttribute->setEnumId($enumId);
        $enumAttribute->setAttributeId($attributeId);
        return $enumAttribute->merge();
    }

    /**
     * @param string $enumCode
     * @param string $code
     * @param string $name
     * @param bool $supportSystem
     * @return null
     * @throws ModelerAbstractEntityException
     */
    public function mergeEnumValue($enumCode, $code, $name, $supportSystem = false) {
        if (empty($enumCode) || empty($code) || empty($name)) return null;
        $enum = ModelerEnumDAO::getInstance()->findByCode($enumCode);
        if ($enum === null) return null;
        $enumValue = ModelerEnumValueDAO::getInstance()->findByCode($code);
        if ($enumValue === null) $enumValue = new ModelerEnumValue();
        $enumId = $enum->getId();
        $enumValue->setEnumId($enumId);
        $enumValue->setCode($code);
        $enumValue->setName($name);
        $enumValue->setSupportSystem($supportSystem);
        return $enumValue->merge();
    }

    /**
     * @param string $enumCode
     * @param string $enumValueCode
     * @param string $attributeCode
     * @param mixed $value
     * @return null
     * @throws ModelerAbstractEntityException
     */
    public function mergeEnumValueAttribute($enumCode, $enumValueCode, $attributeCode, $value) {
        if (empty($enumCode) || empty($enumValueCode) || empty($attributeCode)) return null;
        $enum = ModelerEnumDAO::getInstance()->findByCode($enumCode);
        if ($enum === null) return null;
        $enumValue = ModelerEnumValueDAO::getInstance()->findByCode($enumValueCode);
        if ($enumValue === null) return null;
        $attribute = ModelerAttributeDAO::getInstance()->findByCode($attributeCode);
        if ($attribute === null) return null;
        $enumId = $enum->getId();
        $enumValueId = $enumValue->getId();
        $attributeId = $attribute->getId();
        $dao = ModelerEnumValueAttributeDAO::getInstance();
        $enumValueAttribute = $dao->findByEnumValueAttribute($enumId, $enumValueId, $attributeId);
        if ($enumValueAttribute === null) $enumValueAttribute = new ModelerEnumValueAttribute();
        $enumValueAttribute->setEnumId($enumId);
        $enumValueAttribute->setAttributeId($attributeId);
        $enumValueAttribute->setValue($value);
        return $enumValueAttribute->merge();
    }

    public function addManyToOne($entityCodeSource, $entityCodeTarget)
    {

    }

    public function addOneToOne($entityCodeSource, $entityCodeTarget)
    {

    }

}