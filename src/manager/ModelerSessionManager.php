<?php namespace Volnenko\Modeler\Manager;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSessionManager
{

    /**
     * @var ModelerSessionManager
     */
    private static $instance = null;

    /**
     * @return ModelerSessionManager
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @param string $login
     * @param string $password
     * @return bool
     * @throws ModelerAbstractEntityException
     */
    public function openSession($login, $password)
    {
        if (!$this->checkAccessData($login, $password)) return;
        $user = ModelerUserDAO::getInstance()->getUserByLogin($login);
        if ($user === null) return false;
        $session = new ModelerSession();
        $session->id = ModelerUUID::uuid();
        $session->userId = $user->id;
        $session->timestamp = ModelerDateTime::timestamp();
        $session->ip = $_SERVER['REMOTE_ADDR'];
        setcookie(ModelerAttributeConst::SESSION_ID, $session->id);
        $session = $session->merge();
        return $session !== null;
    }

    /**
     * @param string $login
     * @param string $password
     * @return bool
     * @throws ModelerAbstractEntityException
     */
    public function checkAccessData($login, $password) {
        if (empty($login) || empty($password)) return false;
        $user = ModelerUserManager::getInstance()->getUserByLogin($login);
        if ($user === null || empty($user->passwordHash)) return false;
        $passwordHash = ModelerPasswordUtil::getPasswordHash($password);
        if (empty($passwordHash)) return false;
        return $user->passwordHash === $passwordHash;
    }


    /**
     * @return ModelerAbstractEntity|null
     */
    public function getSession() {
        if (!isset($_COOKIE)) return;
        if (!array_key_exists(ModelerAttributeConst::SESSION_ID, $_COOKIE)) return null;
        $sessionId = $_COOKIE[ModelerAttributeConst::SESSION_ID];
        if (empty($sessionId)) return null;
        try {
            return ModelerSessionDAO::getInstance()->findOne($sessionId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return bool
     */
    public function hasSession() {
        return $this->getSession() !== null;
    }

    public function closeSession()
    {
        if (!isset($_COOKIE)) return;
        if (!array_key_exists(ModelerAttributeConst::SESSION_ID, $_COOKIE)) return;
        $sessionId = $_COOKIE[ModelerAttributeConst::SESSION_ID];
        if (empty($sessionId)) return;
        ModelerSessionDAO::getInstance()->removeById($sessionId);
        setcookie(ModelerAttributeConst::SESSION_ID, '');
    }

    public function closeSessions() {

    }

}