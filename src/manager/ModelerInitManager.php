<?php namespace Volnenko\Modeler\Manager;

use Volnenko\Modeler\Error\ModelerAbstractException;
use Volnenko\Modeler\Manager\ModelerUserManager;
use Volnenko\Modeler\Manager\ModelerLocaleManager;
use Volnenko\Modeler\Manager\ModelerStorageManager;
use Volnenko\Modeler\Manager\ModelerSettingManager;
use Volnenko\Modeler\Dao\ModelerRoleDAO;
use Volnenko\Modeler\Constant\ModelerSettingConst;
use Volnenko\Modeler\Locale\ModelerLocalization;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerInitManager
{

    /**
     * @var ModelerInitManager
     */
    private static $instance = null;

    /**
     * @return ModelerInitManager
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @throws ModelerInitException
     */
    public function init()
    {
        try {
            $this->initStorage();
            $this->initScheme();
            $this->initUserRole();
            $this->initUser();
            $this->initLocale();
            $this->initSettings();
        } catch (ModelerAbstractException $e) {
            throw new ModelerInitException($e);
        }
    }

    /**
     * @throws ModelerAbstractEntityException
     */
    private function initUserRole() {
        ModelerRoleDAO::getInstance()->mergeUserTypes(ModelerRoleType::VALUES());
    }

    private function initStorage() {
        ModelerStorageManager::getInstance()->init();
    }

    private function initSettings() {
        ModelerSettingManager::getInstance()->initSettingValueString(
            ModelerSettingConst::SECRET_KEY, ModelerUUID::uuid(), 'СЕКРЕТНЫЙ КЛЮЧ'
        );
        ModelerSettingManager::getInstance()->initSettingValueString(
            ModelerSettingConst::PASSWORD_SALT, ModelerUUID::uuid(), 'СОЛЬ ПАРОЛЯ'
        );
        ModelerSettingManager::getInstance()->initSettingValueString(
            ModelerSettingConst::PASSWORD_HASH_COUNT, 101, 'КОЛ-ВО ХЕШЕЙ ПАРОЛЯ'
        );
        ModelerSettingManager::getInstance()->initSettingValueInteger(
            ModelerSettingConst::PAGE_LIMIT, 20, 'РАЗМЕР СТРАНИЦЫ'
        );
        ModelerSettingManager::getInstance()->initSettingValueString(
            ModelerSettingConst::SITE_NAME, 'MODELER', 'НАЗВАНИЕ САЙТА'
        );
    }

    public function refreshSecretKey() {
        ModelerSettingManager::getInstance()->setSettingValue(
            ModelerSettingConst::SECRET_KEY, ModelerUUID::uuid()
        );
    }

    /**
     * @throws ModelerAbstractEntityException
     */
    private function initLocale()
    {
        ModelerLocaleManager::getInstance()->addLocale('RU', 'Русский');
        ModelerLocaleManager::getInstance()->addLocale('EN', 'Английский');

        ModelerLocalization::getInstance()
            ->locale('РУССКИЙ', 'RU')

            ->locale('АНГЛИЙСКИЙ', 'EN')
                ->key('ПРИМЕНИТЬ')->value('APPLY')
                ->key('НАСТРОЙКИ')->value('SETTINGS')
                ->key('СТАТИСТИКА')->value('STATISTIC')
                ->key('ЛОГИН')->value('LOGIN')
                ->key('ПАРОЛЬ')->value('PASSWORD')
                ->key('СУЩНОСТЬ')->value('ENTITY')
                ->key('СУЩНОСТИ')->value('ENTITIES')
                ->key('ПЕРЕЧИСЛЕНИЯ')->value('ENUMS')
                ->key('ПЕРЕЧИСЛЕНИЕ')->value('ENUM')
                ->key('АТРИБУТЫ')->value('ATTRIBUTES')
                ->key('АТРИБУТ')->value('ATTRIBUTE')
                ->key('НАИМЕНОВАНИЕ')->value('TITLE')
                ->key('НАЗВАНИЕ')->value('NAME')
                ->key('ЗНАЧЕНИЯ')->value('VALUES')
                ->key('ЗНАЧЕНИЕ')->value('VALUE')
                ->key('ДОБАВИТЬ')->value('CREATE')
                ->key('УДАЛИТЬ')->value('REMOVE')
                ->key('ПРАВКА')->value('EDIT')
                ->key('ОБНОВИТЬ')->value('REFRESH')
                ->key('ЗАПИСЬ')->value('RECORD')
                ->key('ЗАПИСИ')->value('RECORDS')
                ->key('КОД')->value('ID')
                ->key('КЛЮЧ')->value('KEY')
                ->key('ЛОКАЛЬ')->value('LOCALE')
                ->key('ФАЙЛ')->value('FILE')
                ->key('КАРТИНКИ')->value('IMAGES')
                ->key('КАРТИНКА')->value('IMAGE');
    }

    /**
     * @throws ModelerAbstractEntityException
     * @throws ModelerEmailEmptyException
     * @throws ModelerLoginEmptyException
     * @throws ModelerPasswordEmptyException
     * @throws ModelerUserExistsException
     */
    private function initUser()
    {
        if (!ModelerUserDAO::getInstance()->isEmpty()) return;
        ModelerUserManager::getInstance()->createUser('admin', 'admin', 'admin@admin.ru', ModelerRoleType::ADMINISTRATOR);
        ModelerUserManager::getInstance()->createUser('editor', 'editor', 'editor@editor.ru', ModelerRoleType::EDITOR);
        ModelerUserManager::getInstance()->createUser('test', 'test', 'test@test.ru');
    }

    private function initScheme()
    {
        ModelerScheme::getInstance()
            ->attributeString('id', 'ID')
            ->attributeString('externalId', 'Внешений ID')
            ->attributeString('login', 'Логин')
            ->attributeString('password', 'Пароль')
            ->attributeString('passwordHash', 'Хеш пароля')
            ->attributeString('name', 'Название')
            ->attributeString('message', 'Сообщение')
            ->attributeString('email', 'E-mail')
            ->attributeString('description', 'Описание')
            ->attributeString('ip', 'IP')
            ->attributeString('firstName', 'Имя')
            ->attributeString('lastName', 'Фамилия')
            ->attributeString('middleName', 'Отчество')
            ->attributeString('fileName', 'Название файла')
            ->attributeString('contentType', 'Тип контента')
            ->attributeString('host', 'Host')
            ->attributeString('userAgent', 'User Agent')

            ->attributeBoolean('locked', 'Блокировка')

            ->attributeLong('version', 'Версия')
            ->attributeLong('size', 'Размер')

            ->attributeDateTime('dateStart', 'Дата начала')
            ->attributeDateTime('dateEnd', 'Дата окончания')
            ->attributeDateTime('created', 'Создано')
            ->attributeDateTime('updated', 'Обновлено')
            ->attributeDateTime('birthday', 'Дата рождения')

            ->entity('User', 'Пользователь')
                ->entityAttribute('login')
                ->entityAttribute('passwordHash')
                ->entityAttribute('email')
                ->entityAttribute('firstName')
                ->entityAttribute('lastName')
                ->entityAttribute('middleName')
                ->entityAttribute('birthday')
                ->entityAttribute('locked')

            ->entity('Session', 'Сессия')
                ->entityAttribute('ip')
                ->entityAttribute('host')
                ->entityAttribute('userAgent')

            ->entity('Setting', 'Настройка')
                ->entityAttribute('name')
                ->entityAttribute('code')

            ->entity('SettingValue', 'Значение настройки')

            ->entity('Role', 'Роль')
                ->entityAttribute('name')

            ->entity('UserRole', 'Роли пользователя')
            ->entity('Log', 'Логи')
                ->entityAttribute('message')

            ->entity('Folder', 'Папка')
                ->entityAttribute('name')
                ->entityAttribute('description')

            ->entity('File', 'Файл')
                ->entityAttribute('name')
                ->entityAttribute('fileName')
                ->entityAttribute('size')
                ->entityAttribute('contentType')

            ->entity('Image', 'Картинка')
                ->entityAttribute('name')
                ->entityAttribute('fileName')
                ->entityAttribute('size')
                ->entityAttribute('contentType')

            ->entity('Locale','Локаль')
                ->entityAttribute('name')
                ->entityAttribute('code')

            ->entity('LocaleKey', 'Фраза локализации')
            ->entity('LocaleKeyValue', 'Перевод фразы локализации')

            ->entity('Tag', 'Хештег')
                ->entityAttribute('name')

            ->enum('LogType', 'Тип логов')
                ->enumValue('INFO','Info')
                ->enumValue('WARN', 'Warn')
                ->enumValue('TRACE', 'Trace')
                ->enumValue('ERROR', 'Error')

            ->enum('RoleType', 'Тип роли пользователя')
                ->enumValue('ADMIN', 'Администратор')
                ->enumValue('USER','Пользователь')

        ->system()
        ->store();
    }

}