<?php namespace Volnenko\Modeler\Manager;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerStorageManager
{

    /**
     * @var ModelerStorageManager
     */
    private static $instance = null;

    /**
     * @return ModelerStorageManager
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @return string
     */
    private function getFolderBase() {
        try {
            $reflection = new ReflectionClass(Modeler::getInstance());
            $fileName = $reflection->getFilename();
            return (dirname(dirname($fileName)));
        } catch (ReflectionException $e) {
            return './';
        }
    }

    /**
     * @return string
     */
    private function getFolderStorage() {
        return $this->getFolderBase().ModelerStorageConst::FOLDER_STORAGE;
    }

    /**
     * @param $entityName
     * @return string
     */
    private function getFolderEntity($entityName) {
        return $this->getFolderStorage().'/'.$entityName;
    }

    /**
     * @param string $folder
     */
    private function createFolder($folder) {
        if (is_dir($folder)) return;
        mkdir($folder);
    }

    /**
     * @param $entity
     * @return string|null
     */
    public function getEntityName(ModelerAbstractEntity $entity) {
        try {
            $className = get_class($entity);
            return $this->getEntityNameByClass($className);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param ModelerAbstractEntity $record
     * @return ModelerEntityInfoDTO
     * @throws ModelerAbstractEntityException
     */
    private function parseEntity(ModelerAbstractEntity $record) {
        if ($record === null) throw new ModelerNotEntityException();
        $entityCheck = $record instanceof ModelerAbstractEntity;
        if (!$entityCheck) throw new ModelerNotEntityException();
        $entityClass = $this->getEntityName($record);
        if (empty($entityClass)) throw new ModelerEntityNameNotFoundException();
        $entityId = $record->getId();
        if (empty($entityId)) throw new ModelerEntityIdEmptyException();
        return $this->getEntityInfo($entityClass, $entityId);
    }

    /**
     * @param string $entityClass
     * @param string $recordId
     * @return ModelerEntityInfoDTO
     */
    private function getEntityInfo($entityClass, $recordId) {
        $result = new ModelerEntityInfoDTO();
        $result->setEntityFolder($this->getFolderEntity($entityClass));
        $result->setRecordFolder($result->getEntityFolder() . '/' . $recordId);
        $result->setRecordFile($result->getRecordFolder() . '/record-data');
        $result->setMetaFile($result->getRecordFolder() . '/record-meta');
        $result->setEntityName($entityClass);
        $result->setEntityId($recordId);
        return $result;
    }

    /**
     * @param string $entityId
     * @param string $recordId
     * @return ModelerEntityInfoDTO
     * @throws ModelerAbstractEntityException
     */
    private function getInfoByEntityId($entityId, $recordId) {
        $entity = ModelerEntityDAO::getInstance()->findOne($entityId);
        if ($entity === null) return null;
        $entityClass = $entity->getCode();
        return $this->getEntityInfo($entityClass, $recordId);
    }

    /**
     * @param string $entityName
     * @param string $recordId
     * @return ModelerEntityInfoDTO
     * @throws ModelerAbstractEntityException
     */
    private function getInfoByEntityName($entityName, $recordId) {
        $entity = ModelerEntityDAO::getInstance()->findByCode($entityName);
        if ($entity === null) return null;
        $entityClass = $entity->getCode();
        return $this->getEntityInfo($entityClass, $recordId);
    }

    /**
     * @param string $entityClass
     * @param string $entityId
     * @return string
     */
    private function getFolderRecord($entityClass, $entityId) {
        $entityFolder = $this->getEntityFolderByClass($entityClass);
        return $entityFolder . '/'.$entityId;
    }

    /**
     * @param string $entityClass
     * @param string $entityId
     * @return string
     */
    private function getFileContent($entityClass, $entityId) {
        return $this->getFolderRecord($entityClass, $entityId) . '/content-data';
    }

    /**
     * @param string $entityClass
     * @param string $entityId
     */
    public function removeEntityContent($entityClass, $entityId) {
        $exists = $this->exists($entityClass, $entityId);
        if (!$exists) return;
        $fileContent = $this->getFileContent($entityClass, $entityId);
        unlink($fileContent);
    }

    /**
     * @param string $entityClass
     * @param string $entityId
     * @return bool|null|string
     */
    public function getEntityContent($entityClass, $entityId) {
        if (!$this->hasEntityContent($entityClass, $entityId)) return null;
        $fileContent = $this->getFileContent($entityClass, $entityId);
        return file_get_contents($fileContent);
    }

    /**
     * @param string $entityClass
     * @param string $entityId
     * @return bool
     */
    public function hasEntityContent($entityClass, $entityId) {
        if (!$this->exists($entityClass, $entityId)) return false;
        $fileContent = $this->getFileContent($entityClass, $entityId);
        return file_exists($fileContent);
    }

    /**
     * @param string $entityClass
     * @param string $entityId
     * @param mixed $content
     */
    public function setEntityContent($entityClass, $entityId, $content) {
        $exists = $this->exists($entityClass, $entityId);
        if (!$exists) return;
        $fileContent = $this->getFileContent($entityClass, $entityId);
        file_put_contents($fileContent, $content);
    }

    /**
     * @param $entityClass
     * @return string|null
     */
    private function getEntityNameByClass($entityClass) {
        if (empty($entityClass)) return null;
        return preg_replace('/^Modeler/', '', $entityClass);
    }

    /**
     * @param string $entityClass
     * @return string
     */
    private function getEntityFolderByClass($entityClass) {
        $entityName = $this->getEntityNameByClass($entityClass);
        return $this->getFolderEntity($entityName);
    }

    /**
     * @param string $folder
     */
    private function disableHTAccess($folder) {
        if (empty($folder)) return;
        if (!is_dir($folder)) return;
        $httpAccessFile = $folder.'/.htaccess';
        $data = 'deny from all';
        file_put_contents($httpAccessFile, $data);
    }

    public function destroy() {
        $folderStorage = $this->getFolderStorage();
        ModelerFileUtil::deleteDir($folderStorage);
    }

    /**
     * @param string $entityClass
     * @return array
     */
    public function getEntityIds($entityClass) {
        $entityName = $this->getEntityNameByClass($entityClass);
        $folderEntity = $this->getFolderEntity($entityName) . '/';
        $folders = array_filter(glob($folderEntity.'*'), 'is_dir');
        $result = array();
        foreach ($folders as $folder) $result[] = basename($folder);
        return $result;
    }

    /**
     * @param ModelerAbstractEntity $entity
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function persist(ModelerAbstractEntity $entity) {
        if ($this->exists(get_class($entity), $entity->getId())) throw new ModelerEntityRecordExistsException();
        return $this->merge($entity);
    }

    /**
     * @param String $entityId
     * @param stdClass $object
     * @throws ModelerAbstractEntityException
     */
    public function mergeRowByEntityId($entityId, $object) {
        if ($object === null) return;
        if (empty($entityId)) return;
        $recordId = $object->id;
        $entityInfo = $this->getInfoByEntityId($recordId, $entityId);
        if ($entityInfo === null) return;
        $this->flushRaw($object, $entityInfo);
    }

    /**
     * @param String $entityClass
     * @param stdClass $object
     * @throws ModelerAbstractEntityException
     */
    public function mergeRaw($entityClass, $object) {
        if ($object === null) return;
        if (empty($entityClass)) return;
        $recordId = $object->id;
        $entityInfo = $this->getInfoByEntityName($entityClass, $recordId);
        if ($entityInfo === null) return;
        $this->flushRaw($object, $entityInfo);
    }

    /**
     * @param ModelerAbstractEntity $entity
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function merge(ModelerAbstractEntity $entity) {
        if ($entity === null) return null;
        $entityInfo = $this->parseEntity($entity);
        return $this->flushRaw($entity, $entityInfo);
    }

    /**
     * @param $entityObject
     * @param ModelerEntityInfoDTO $entityInfo
     * @return mixed
     */
    private function flushRaw($entityObject, $entityInfo) {
        ModelerDataUtil::initEntity($entityObject);
        $this->createFolder($entityInfo->getEntityFolder());
        $this->createFolder($entityInfo->getRecordFolder());
        $entityJSON = ModelerDataUtil::objectToJson($entityObject);
        file_put_contents($entityInfo->getRecordFile(), $entityJSON);
        $entityMetaJSON = ModelerDataUtil::objectToMetaJson($entityObject);
        file_put_contents($entityInfo->getMetaFile(), $entityMetaJSON);
        return $entityObject;
    }

    /**
     * @param string $entityClass
     * @param string $recordId
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function findOne($entityClass, $recordId) {
        if (empty($recordId)) return null;
        if (!$this->exists($entityClass, $recordId)) return null;
        // TODO check class exists with Modeler Prefix
        $entity = new $entityClass();
        $entity->setId($recordId);
        $this->refresh($entity);
        return $entity;
    }

    /**
     * @param string $entityClass
     * @param string $recordId
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function findOneRaw($entityClass, $recordId) {
        if (empty($recordId)) return null;
        if (!$this->exists($entityClass, $recordId)) return null;
        $record = new stdClass();
        $record->id = $recordId;
        $entityInfo = $this->getInfoByEntityName($entityClass, $recordId);
        return $this->loadObject($record, $entityInfo);
    }

    /**
     * @param string $entityClass
     * @param int $start
     * @param int $limit
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAll($entityClass, $start = null, $limit = null) {
        if (empty($entityClass)) return array();
        $entityIds = self::ids($entityClass, $start, $limit);
        $result = array();
        foreach ($entityIds as $entityId) {
            $entity = $this->findOne($entityClass, $entityId);
            if ($entity === null) continue;
            $result[] = $entity;
        }
        return $result;
    }

    /**
     * @param string $entityClass
     * @param int $start
     * @param int $limit
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAllRaw($entityClass, $start = null, $limit = null) {
        if (empty($entityClass)) return array();
        $entityIds = self::ids($entityClass, $start, $limit);
        $result = array();
        foreach ($entityIds as $entityId) {
            $entity = $this->findOneRaw($entityClass, $entityId);
            if ($entity === null) continue;
            $result[] = $entity;
        }
        return $result;
    }

    /**
     * @param string $entityClass
     * @param int $start
     * @param int $limit
     * @return array
     */
    private function ids($entityClass, $start = null, $limit = null) {
        if (empty($entityClass)) return array();
        $entityName = $this->getEntityNameByClass($entityClass);
        $ids = $this->getEntityIds($entityName);
        return ModelerArrayUtil::paging($ids, $start, $limit);
    }

    /**
     * @param ModelerAbstractEntity $entity
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function refresh(ModelerAbstractEntity $entity) {
        if ($entity === null) return null;
        $entityInfo = $this->parseEntity($entity);
        return $this->loadObject($entity, $entityInfo);
    }

    /**
     * @param $entityObject
     * @param ModelerEntityInfoDTO $entityInfo
     * @return mixed
     * @throws ModelerEntityRecordCorruptException
     */
    private function loadObject($entityObject, $entityInfo) {
        if (!is_file($entityInfo->getRecordFile())) throw new ModelerEntityRecordCorruptException();
        $dataJSON = file_get_contents($entityInfo->getRecordFile());
        if (!is_file($entityInfo->getMetaFile())) throw new ModelerEntityRecordCorruptException();
        $metaJSON = file_get_contents($entityInfo->getMetaFile());
        return ModelerDataUtil::loadRowData($entityObject, $dataJSON, $metaJSON);
    }

    /**
     * @param ModelerAbstractEntity $entity
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function remove(ModelerAbstractEntity $entity) {
        if ($entity === null) return null;
        $entityInfo = $this->parseEntity($entity);
        $recordFolder = $entityInfo->getRecordFolder();
        ModelerFileUtil::deleteDir($recordFolder);
        return $entity;
    }

    /**
     * @param string $entityClass
     * @param string $entityId
     * @return bool
     */
    public function exists($entityClass, $entityId) {
        if (empty($entityClass)) return null;
        $folderRecord = $this->getFolderRecord($entityClass, $entityId);
        return is_dir($folderRecord);
    }

    /**
     * @param string $entityClass
     */
    public function removeAll($entityClass) {
        $entityName = $this->getEntityNameByClass($entityClass);
        $entityFolder = $this->getFolderEntity($entityName);
        ModelerFileUtil::deleteDir($entityFolder);
    }

    /**
     * @param string $entityClass
     * @param string $entityId
     */
    public function removeById($entityClass, $entityId) {
        if (empty($entityClass)) return;
        if (empty($entityId)) return;
        if (!$this->exists($entityClass, $entityId)) return;
        $folderRecord = $this->getFolderRecord($entityClass, $entityId);
        ModelerFileUtil::deleteDir($folderRecord);
    }

    public function init() {
        $this->createFolder($this->getFolderStorage());
        $this->disableHTAccess($this->getFolderStorage());
    }

}