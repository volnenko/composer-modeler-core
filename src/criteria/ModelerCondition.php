<?php

namespace Volnenko\Modeler\Criteria;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerCondition
{

    /**
     * @var ModelerCriteria
     */
    private $criteria;

    /**
     * @var ModelerPredicate
     */
    private $predicate;

    /**
     * @var string
     */
    private $predicateTypeId;

    /**
     * @var
     */
    private $attribute;

    /**
     * @var
     */
    private $value;

    /**
     * @var string
     */
    private $conditionTypeId;

    /**
     * ModelerCondition constructor.
     * @param ModelerCriteria $criteria
     * @param ModelerPredicate $predicate
     * @param string $conditionTypeId
     * @param string $attribute
     * @param int|string|double|float|DateTime|bool|boolean $value
     */
    public function __construct(
        ModelerCriteria $criteria,
        ModelerPredicate $predicate,
        $conditionTypeId,
        $attribute,
        $value = null
    )
    {
        $this->criteria = $criteria;
        $this->conditionTypeId = $conditionTypeId;
        $this->predicate = $predicate;
        $this->attribute = $attribute;
        $this->value = $value;
    }

    /**
     * @return ModelerCriteria
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @return string
     */
    public function attribute()
    {
        return $this->attribute;
    }

    /**
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionEquals($attribute, $value) {
        return $this->predicate->conditionEquals($attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionNotEquals($attribute, $value) {
        return $this->predicate->conditionNotEquals($attribute, $value);
    }

    /**
     * @param $attribute
     * @return ModelerCondition
     */
    public function conditionIsNull($attribute) {
        return $this->predicate->conditionIsNull($attribute);
    }

    /**
     * @param $attribute
     * @return ModelerCondition
     */
    public function conditionNotNull($attribute) {
        return $this->predicate->conditionNotNull($attribute);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionLessThan($attribute, $value) {
        return $this->predicate->conditionLessThan($attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionLessEquals($attribute, $value) {
        return $this->predicate->conditionLessEquals($attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionGreaterThan($attribute, $value) {
        return $$this->predicate->conditionGreaterThan($attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionGreaterEquals($attribute, $value) {
        return $this->predicate->conditionGreaterEquals($attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionContains($attribute, $value) {
        return $this->predicate->conditionContains($attribute, $value);
    }

    /**
     * @return string
     */
    public function predicateTypeId()
    {
        return $this->predicateTypeId;
    }

    /**
     * @return ModelerPredicate
     */
    public function predicateAnd() {
        return $this->predicate->predicateAnd();
    }

    /**
     * @return ModelerPredicate
     */
    public function predicateOr() {
        return $this->predicate->predicateOr();
    }

    /**
     * @return ModelerCriteria
     */
    public function criteria() {
        return $this->criteria;
    }

    /**
     * @return ModelerPredicate
     */
    public function predicate() {
        return $this->predicate;
    }

    /**
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findOne() {
        return $this->predicate->findOne();
    }

    /*
     * @return array
     */
    /**
     * @param int $start
     * @param int $limit
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAll($start = null, $limit = null) {
        return $this->predicate->findAll($start, $limit);
    }

    /**
     * @return int
     * @throws ModelerAbstractEntityException
     */
    public function count() {
        return $this->predicate->count();
    }

    /**
     * @param ModelerAbstractEntity $entity
     * @return bool
     */
    public function validate($entity) {
        if (!$entity->hasAttribute($this->attribute)) return false;
        $attributeValue = $entity->attributeValue($this->attribute);
        if ($this->conditionTypeId === ModelerConditionType::EQUALS) return $this->value === $attributeValue;
        if ($this->conditionTypeId === ModelerConditionType::NOT_EQUALS) return $this->value !== $attributeValue;
        if ($this->conditionTypeId === ModelerConditionType::IS_NULL) return $this->value === null;
        if ($this->conditionTypeId === ModelerConditionType::NOT_NULL) return $this->value !== null;
        if ($this->conditionTypeId === ModelerConditionType::LESS_THAN) return $this->value < $attributeValue;
        if ($this->conditionTypeId === ModelerConditionType::LESS_EQUALS) return $this->value <= $attributeValue;
        if ($this->conditionTypeId === ModelerConditionType::GREATER_THAN) return $this->value > $attributeValue;
        if ($this->conditionTypeId === ModelerConditionType::GREATER_EQUALS) return $this->value >= $attributeValue;
        if ($this->conditionTypeId === ModelerConditionType::CONTAINS)  {
            if (empty($this->value)) return true;
            if (empty($attributeValue)) return false;
            return strpos($attributeValue, $this->value) !== false;
        }
        return false;
    }

    /**
     * @return int|null
     */
    public function getStart()
    {
        return $this->predicate->getStart();
    }

    /**
     * @param int|null $start
     * @return ModelerCriteria
     */
    public function setStart($start)
    {
        return $this->predicate->setStart($start);
    }

    /**
     * @return int|null
     */
    public function getLimit()
    {
        return $this->predicate->getLimit();
    }

    /**
     * @param int|null $limit
     * @return ModelerCriteria
     */
    public function setLimit($limit)
    {
        return $this->predicate->setLimit($limit);
    }

}