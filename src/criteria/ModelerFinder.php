<?php namespace Volnenko\Modeler\Criteria;

use Volnenko\Modeler\Util\ModelerArrayUtil;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerFinder
{

    /**
     * @var ModelerCriteria
     */
    private $criteria;

    /**
     * ModelerFinder constructor.
     * @param ModelerCriteria $criteria
     */
    public function __construct($criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * @param $array
     * @return $array
     */
    private function sortResult($array) {
        $sorts = $this->criteria->sorts();
        if (empty($sorts)) return $array;
        if (count($sorts) === 1) {
            $sort = $sorts[0];
            reset($sort);
            $sortField = key($sort);
            $sortType = current($sort);
            if ($sortType === SORT_ASC) usort($array, function ($a, $b) use ($sortField) {
                return strnatcmp($a->$sortField, $b->$sortField);
            });
            if ($sortType === SORT_DESC) usort($array, function ($a, $b) use ($sortField) {
                return strnatcmp($b->$sortField, $a->$sortField);
            });
        }
        else {
            //TODO CHECK SORTS FOR ANY KEYS
            $dynamicSort = array();
            foreach ($sorts as $sort) {
                foreach ($sort as $sortField => $sortType) {
                    $dynamicSort[] = array_column($array, $sortField);
                    $dynamicSort[] = $sortType;
                }
            }
            $param = array_merge($dynamicSort, array($array));
            call_user_func_array('array_multisort', $param);
        }
        return $array;
    }

    /**
     * @param ModelerCriteria $criteria
     * @return array
     * @throws ModelerAbstractEntityException
     */
    private function find(ModelerCriteria $criteria) {
        $entityClass = $criteria->entityClass();
        if (empty($entityClass)) return array();
        $ids = ModelerEntityManager::getInstance()->ids($entityClass);
        $result = array();
        $skipCount = 0;
        $start = $criteria->getStart();
        $startSupport = is_int($start) && $start > 0;
        $limit = $criteria->getLimit();
        $limitSupport = is_int($limit) && $limit > 0;
        $sortSupport = $criteria->hasSort();
        $em = ModelerEntityManager::getInstance();
        $raw = $criteria->getRaw();
        foreach ($ids as $id) {
            $entity = null;
            $entity = $raw ? $em->findOneRaw($entityClass, $id) : $em->findOne($entityClass, $id);
            $validate = $criteria->validate($entity);

            # Apply criteria start to findById results
            if (!$sortSupport && $validate && $startSupport && $skipCount < $start) {
                $skipCount++;
                continue;
            }
            if ($validate) $result[] = $entity;

            # Apply criteria limit to findById results
            if (!$sortSupport && $limitSupport && count($result) >= $limit) break;
        }
        if ($sortSupport) $result = $this->sortResult($result);
        $supportPaging = $startSupport || $limitSupport;
        if ($sortSupport && $supportPaging) return ModelerArrayUtil::paging($result, $start, $limit);
        return $result;
    }

    /**
     * @return ModelerAbstractEntity|null
     * @throws ModelerAbstractEntityException
     */
    public function findOne() {
        $this->criteria->setLimit(1);
        $result = $this->find($this->criteria);
        if (count($result) === 0) return null;
        return $result[0];
    }

    /**
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAll() {
        return $this->find($this->criteria);
    }

    /**
     * @param ModelerCriteria $criteria
     * @return ModelerFinder
     */
    public static function create(ModelerCriteria $criteria) {
        return new ModelerFinder($criteria);
    }

}