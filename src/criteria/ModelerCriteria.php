<?php namespace Volnenko\Modeler\Criteria;

use Volnenko\Modeler\Constant\ModelerSortType as ModelerSortType;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerCriteria
{

    /**
     * @var string
     */
    private $entityClass = null;

    /**
     * @var int|null
     */
    private $start = 0;

    /**
     * @var int|null
     */
    private $limit = null;

    /**
     * @var array
     */
    private $predicates = array();

    /**
     * @var array
     */
    private $sorts = array();

    /**
     * @var bool
     */
    private $raw = false;


    /**
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return ModelerCriteria $this
     */
    public function sort($sortAttribute, $sortTypeId) {
        if (empty($sortAttribute) || empty($sortTypeId)) return $this;
        if ($sortTypeId === ModelerSortType::ASC) return $this->sortAscending($sortAttribute);
        if ($sortTypeId === ModelerSortType::DESC) return $this->sortDescending($sortAttribute);
        return $this;
    }

    private function sortInternal($code, $sortType) {
        $this->sorts[] = array($code => $sortType);
        return $this;
    }

    /**
     * @param string $code
     * @return ModelerCriteria
     */
    public function sortAscending($code) {
        return $this->sortInternal($code, SORT_ASC);
    }

    /**
     * @param string $code
     * @return ModelerCriteria
     */
    public function sortDescending($code) {
        return $this->sortInternal($code, SORT_DESC);
    }

    /**
     * @return array
     */
    public function sorts()
    {
        return $this->sorts;
    }

    /**
     * @return bool
     */
    public function hasSort() {
        return count($this->sorts) > 0;
    }

    /**
     * ModelerCriteria constructor.
     * @param string $entityClass
     */
    public function __construct($entityClass)
    {
        $this->entityClass = $entityClass;
    }

    /**
     * @return string
     */
    public function entityClass()
    {
        return $this->entityClass;
    }

    /**
     * @param string $entityClass
     * @return ModelerCriteria
     */
    public static function create($entityClass) {
        $criteria = new ModelerCriteria($entityClass);
        return $criteria;
    }

    /**
     * @param string $predicateTypeId
     * @return ModelerPredicate
     */
    private function predicate($predicateTypeId) {
        $predicate = new ModelerPredicate($this, $predicateTypeId);
        $this->predicates[] = $predicate;
        return $predicate;
    }

    /**
     * @return array
     */
    public function predicates() {
        return $this->predicates;
    }

    /**
     * @return ModelerPredicate
     */
    public function predicateAnd() {
        return $this->predicate(ModelerPredicateType::LOGICAL_AND);
    }

    /**
     * @return ModelerPredicate
     */
    public function predicateOr() {
        return $this->predicate(ModelerPredicateType::LOGICAL_OR);
    }

    /**
     * @return int|null
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param int|null $start
     * @return ModelerCriteria
     */
    public function setStart($start)
    {
        $this->start = $start;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     * @return ModelerCriteria
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param $entity
     * @return bool
     */
    public function validate($entity) {
        foreach ($this->predicates as $predicate) if (!$predicate->validate($entity)) return false;
        return true;
    }

    /**
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findOne() {
        return ModelerFinder::create($this)->findOne();
    }

    /**
     * @param int $start
     * @param int $limit
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAll($start = null, $limit = null) {
        $this->setStart($start);
        $this->setLimit($limit);
        return ModelerFinder::create($this)->findAll();
    }

    /**
     * @return bool
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * @param bool $raw
     * @return ModelerCriteria
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;
        return $this;
    }


    /**
     * @return int
     * @throws ModelerAbstractEntityException
     */
    public function count() {
        return count($this->findAll());
    }

}