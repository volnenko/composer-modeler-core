<?php namespace Volnenko\Modeler\Criteria;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerPredicate
{
    /**
     * @var ModelerCriteria
     */
    private $criteria;

    /**
     * @var string
     */
    private $predicateTypeId;

    /**
     * @var array
     */
    private $conditions = array();

    /**
     * @return array
     */
    public function conditions() {
        return $this->conditions;
    }

    /**
     * ModelerPredicate constructor.
     * @param ModelerCriteria $criteria
     * @param $predicateTypeId
     */
    public function __construct(ModelerCriteria $criteria, $predicateTypeId)
    {
        $this->criteria = $criteria;
        $this->predicateTypeId = $predicateTypeId;
    }

    /**
     * @param string $conditionTypeId
     * @param string $attribute
     * @param null $value
     * @return ModelerCondition
     */
    private function condition($conditionTypeId, $attribute, $value = null) {
        $condition = new ModelerCondition($this->criteria, $this, $conditionTypeId, $attribute, $value);
        $this->conditions[] = $condition;
        return $condition;
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionEquals($attribute, $value) {
        return $this->condition(ModelerConditionType::EQUALS, $attribute, $value);
    }

    /**
     * @param $attribute
     * @return ModelerCondition
     */
    public function conditionIsNull($attribute) {
        return $this->condition(ModelerConditionType::IS_NULL, $attribute);
    }

    /**
     * @param $attribute
     * @return ModelerCondition
     */
    public function conditionNotNull($attribute) {
        return $this->condition(ModelerConditionType::IS_NULL, $attribute);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionLessThan($attribute, $value) {
        return $this->condition(ModelerConditionType::LESS_THAN, $attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionLessEquals($attribute, $value) {
        return $this->condition(ModelerConditionType::LESS_THAN, $attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionGreaterThan($attribute, $value) {
        return $this->condition(ModelerConditionType::GREATER_THAN, $attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionGreaterEquals($attribute, $value) {
        return $this->condition(ModelerConditionType::GREATER_EQUALS, $attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionContains($attribute, $value) {
        return $this->condition(ModelerConditionType::CONTAINS, $attribute, $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @return ModelerCondition
     */
    public function conditionNotEquals($attribute, $value) {
        return $this->condition(ModelerConditionType::NOT_EQUALS, $attribute, $value);
    }

    /**
     * @return ModelerPredicate
     */
    public function predicateAnd() {
        return $this->criteria->predicateAnd();
    }

    /**
     * @return ModelerPredicate
     */
    public function predicateOr() {

        return $this->criteria->predicateOr();
    }

    /**
     * @return ModelerCriteria
     */
    public function criteria() {
        return $this->criteria;
    }

    /**
     * @return ModelerAbstractEntity
     * @throws ModelerAbstractEntityException
     */
    public function findOne() {
        return $this->criteria->findOne();
    }

    /*
     * @return array
     */
    /**
     * @param int $start
     * @param int $limit
     * @return array
     * @throws ModelerAbstractEntityException
     */
    public function findAll($start = null, $limit = null) {
        return $this->criteria->findAll($start, $limit);
    }

    /**
     * @param ModelerAbstractEntity $entity
     * @return bool
     */
    private function validateOr(ModelerAbstractEntity $entity) {
        $result = false;
        foreach ($this->conditions as $condition) if ($condition->validate($entity)) $result = true;
        return $result;
    }

    /**
     * @param ModelerAbstractEntity $entity
     * @return bool
     */
    private function validateAnd(ModelerAbstractEntity $entity) {
        foreach ($this->conditions as $condition) if (!$condition->validate($entity)) return false;
        return true;
    }

    /**
     * @param ModelerAbstractEntity $entity
     * @return bool
     */
    public function validate($entity) {
        if ($this->predicateTypeId === ModelerPredicateType::LOGICAL_OR) return $this->validateOr($entity);
        if ($this->predicateTypeId === ModelerPredicateType::LOGICAL_AND) return $this->validateAnd($entity);
        return false;
    }

    /**
     * @return int|null
     */
    public function getStart()
    {
        return $this->criteria->getStart();
    }

    /**
     * @param int|null $start
     * @return ModelerCriteria
     */
    public function setStart($start)
    {
        return $this->criteria->setStart($start);
    }

    /**
     * @return int|null
     */
    public function getLimit()
    {
        return $this->criteria->getLimit();
    }

    /**
     * @param int|null $limit
     * @return ModelerCriteria
     */
    public function setLimit($limit)
    {
        return $this->criteria->setLimit($limit);
    }

    /**
     * @return int
     * @throws ModelerAbstractEntityException
     */
    public function count() {
        return $this->criteria->count();
    }

}