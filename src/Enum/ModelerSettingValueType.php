<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSettingValueType extends ModelerAbstractEnum
{

    const SINGLE = 'SINGLE';

    const MULTIPLE = 'MULTIPLE';

    const VALUES = array(self::SINGLE, self::MULTIPLE);

    /**
     * @return ModelerSettingValueType
     */
    public static function SINGLE() {
        return new ModelerSettingValueType(self::SINGLE, 'ОДИН');
    }

    /**
     * @return ModelerSettingValueType
     */
    public static function MULTIPLE() {
        return new ModelerSettingValueType(self::MULTIPLE, 'МНОГО');
    }

    /**
     * @return ModelerSettingValueType[]
     */
    public static function VALUES() {
        return array(
            self::SINGLE(),
            self::MULTIPLE()
        );
    }

    /**
     * @param $value
     * @return ModelerSettingValueType|null
     */
    public static function valueOf($value)
    {
        if (empty($value)) return null;
        if (self::SINGLE === $value) return self::SINGLE();
        if (self::MULTIPLE === $value) return self::MULTIPLE();
        return null;
    }

    /**
     * @var string
     */
    var $name = '';

    /**
     * @var string
     */
    var $description = '';

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}