<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSortType
{

    const ASC = 'ASC';

    const DESC = 'DESC';

    const VALUES = array(self::ASC, self::DESC);

    /**
     * @return ModelerSortType
     */
    public static function ASC() {
        return new ModelerSortType(ModelerSortType::ASC, 'ПО ВОЗРАСТАНИЮ');
    }

    /**
     * @return ModelerSortType
     */
    public static function DESC() {
        return new ModelerSortType(ModelerSortType::DESC, 'ПО УБЫВАНИЮ');
    }

    /**
     * @return ModelerSortType[]
     */
    public static function VALUES() {
        return array(self::ASC(), self::DESC());
    }

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * ModelerSortType constructor.
     * @param null|string $code
     * @param null|string $name
     */
    public function __construct($code, $name)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @param $value
     * @return ModelerSortType|null
     */
    public static function valueOf($value) {
        if ($value === self::ASC) return self::ASC();
        if ($value === self::DESC) return self::DESC();
        return null;
    }

}