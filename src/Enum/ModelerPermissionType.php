<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerPermissionType extends ModelerAbstractEnum
{

    const SELECT = 'SELECT';

    const PERSIST = 'PERSIST';

    const UPDATE = 'UPDATE';

    const REMOVE = 'REMOVE';

    const VALUES = array(self::SELECT, self::PERSIST, self::UPDATE, self::REMOVE);

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

}