<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerAttributeType extends ModelerAbstractEnum
{

    const STRING = 'STRING';

    const BOOLEAN = 'BOOLEAN';

    const INTEGER = 'INTEGER';

    const LONG = 'LONG';

    const FLOAT = 'FLOAT';

    const DOUBLE = 'DOUBLE';

    const DATE = 'DATE';

    const ENUM = 'ENUM';

    const ENTITY = 'ENTITY';

    const VALUES = array(
        self::STRING,
        self::BOOLEAN,
        self::INTEGER,
        self::LONG,
        self::FLOAT,
        self::DOUBLE,
        self::DATE,
        self::ENUM,
        self::ENTITY
    );

    /**
     * @return ModelerAttributeType
     */
    public static function STRING()
    {
        return new ModelerAttributeType(self::STRING, self::lc('Строка'));
    }

    /**
     * @return ModelerAttributeType
     */
    public static function INTEGER()
    {
        return new ModelerAttributeType(self::INTEGER, self::lc('Целое число'));
    }

    /**
     * @return ModelerAttributeType
     */
    public static function LONG() {
        return new ModelerAttributeType(self::INTEGER, self::lc('Большое целое число'));
    }

    /**
     * @return ModelerAttributeType
     */
    public static function FLOAT() {
        return new ModelerAttributeType(self::FLOAT, self::lc('Дробное число'));
    }

    /**
     * @return ModelerAttributeType
     */
    public static function DOUBLE() {
        return new ModelerAttributeType(self::DOUBLE, self::lc('Большое дробное число'));
    }

    /**
     * @return ModelerAttributeType
     */
    public static function ENUM()
    {
        return new ModelerAttributeType(self::ENUM, self::lc('Перечисление'));
    }

    /**
     * @return ModelerAttributeType
     */
    public static function DATE()
    {
        return new ModelerAttributeType(self::DATE, self::lc('Дата'));
    }

    /**
     * @return ModelerAttributeType
     */
    public static function BOOLEAN()
    {
        return new ModelerAttributeType(self::BOOLEAN, self::lc('Флаг'));
    }

    /**
     * @return ModelerAttributeType
     */
    public static function ENTITY()
    {
        return new ModelerAttributeType(self::ENTITY, self::lc('Сущность'));
    }

    /**
     * @return ModelerAttributeType[]
     */
    public static function VALUES()
    {
        return array(
            self::STRING(),
            self::BOOLEAN(),
            self::INTEGER(),
            self::LONG(),
            self::FLOAT(),
            self::DOUBLE(),
            self::DATE(),
            self::ENUM(),
            self::ENTITY()
        );
    }

    /**
     * @param $value
     * @return ModelerAttributeType|null
     */
    public static function valueOf($value)
    {
        if (empty($value)) return null;
        if (self::STRING === $value) return self::STRING();
        if (self::BOOLEAN === $value) return self::BOOLEAN();
        if (self::INTEGER === $value) return self::INTEGER();
        if (self::LONG === $value) return self::LONG();
        if (self::FLOAT === $value) return self::FLOAT();
        if (self::DOUBLE === $value) return self::DOUBLE();
        if (self::DATE === $value) return self::DATE();
        if (self::ENUM === $value) return self::ENUM();
        if (self::ENTITY === $value) return self::ENTITY();
        return null;
    }

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $name;

    /**
     * ModelerAttributeType constructor.
     * @param $code
     * @param $name
     */
    public function __construct($code = null, $name = null)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

}