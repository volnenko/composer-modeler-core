<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

abstract class ModelerAbstractEnum
{

    /**
     * @param string|null $value
     * @return string|null
     */
    protected static function lc($value) {
        return $value;
    }

}