<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSettingType extends ModelerAbstractEnum
{

    const STRING = 'STRING';

    const INTEGER = 'INTEGER';

    const LONG = 'LONG';

    const FLOAT = 'FLOAT';

    const DOUBLE = 'DOUBLE';

    const DATE = 'DATE';

    const BOOLEAN = 'BOOLEAN';

    const VALUES = array(
        self::STRING,
        self::INTEGER,
        self::LONG,
        self::FLOAT,
        self::DOUBLE,
        self::DATE,
        self::BOOLEAN,
    );

    /**
     * @return ModelerSettingType[]
     */
    public function VALUES() {
        return array(
            self::STRING(),
            self::BOOLEAN(),
            self::INTEGER(),
            self::LONG(),
            self::FLOAT(),
            self::DOUBLE(),
            self::DATE()
        );
    }

    /**
     * ModelerSettingType constructor.
     * @param null|string $code
     * @param null|string $name
     */
    public function __construct($code, $name)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @param string $value
     * @return null|ModelerSettingType
     */
    public static function valueOf($value) {
        return null;
    }

    /**
     * @return ModelerSettingType
     */
    public static function STRING() {
        return new ModelerSettingType(self::STRING, 'Строка');
    }


    /**
     * @return ModelerSettingType
     */
    public static function INTEGER()
    {
        return new ModelerSettingType(self::INTEGER, self::lc('Целое число'));
    }

    /**
     * @return ModelerSettingType
     */
    public static function LONG() {
        return new ModelerSettingType(self::INTEGER, self::lc('Большое целое число'));
    }

    /**
     * @return ModelerSettingType
     */
    public static function FLOAT() {
        return new ModelerSettingType(self::FLOAT, self::lc('Дробное число'));
    }

    /**
     * @return ModelerSettingType
     */
    public static function DOUBLE() {
        return new ModelerSettingType(self::DOUBLE, self::lc('Большое дробное число'));
    }

    /**
     * @return ModelerSettingType
     */
    public static function DATE()
    {
        return new ModelerSettingType(self::DATE, self::lc('Дата'));
    }

    /**
     * @return ModelerSettingType
     */
    public static function BOOLEAN()
    {
        return new ModelerSettingType(self::BOOLEAN, self::lc('Флаг'));
    }

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

}