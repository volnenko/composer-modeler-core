<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerRoleType extends ModelerAbstractEnum
{

    const ADMINISTRATOR = 'ADMINISTRATOR';

    const EDITOR = 'EDITOR';

    const VALUES = array(self::ADMINISTRATOR);

    /**
     * @return ModelerRoleType[]
     */
    public static function VALUES() {
        return array(
            self::ADMINISTRATOR(),
            self::EDITOR()
        );
    }

    /**
     * ModelerRoleType constructor.
     * @param null|string $code
     * @param null|string $name
     */
    public function __construct($code, $name)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @return ModelerRoleType
     */
    public static function ADMINISTRATOR() {
        return new ModelerRoleType(self::ADMINISTRATOR, self::lc('Администратор'));
    }

    /**
     * @return ModelerRoleType
     */
    public static function EDITOR() {
        return new ModelerRoleType(self::EDITOR, self::lc('Редактор'));
    }

    /**
     * @param $value
     * @return ModelerRoleType|null
     */
    public static function valueOf($value) {
        if ($value === self::ADMINISTRATOR) return self::ADMINISTRATOR();
        return null;
    }

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

}