<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSwitchType extends ModelerAbstractEnum
{

    const ENABLED = 'ENABLED';

    const DISABLED = 'DISABLED';

    const VALUES = array(self::ENABLED, self::DISABLED);

    /**
     * @return ModelerSwitchType[]
     */
    public static function VALUES() {
        return array(
            self::ENABLED(),
            self::DISABLED()
        );
    }

    /**
     * @param $value
     * @return ModelerSwitchType|null
     */
    public static function valueOf($value) {
        if (empty($value)) return null;
        if (self::ENABLED === $value) return self::ENABLED();
        if (self::DISABLED === $value) return self::DISABLED();
        return null;
    }

    /**
     * @return ModelerSwitchType
     */
    public static function ENABLED() {
        return new ModelerSwitchType(self::ENABLED, 'Разрешено');
    }

    /**
     * @return ModelerSwitchType
     */
    public static function DISABLED() {
        return new ModelerSwitchType(self::DISABLED, 'Запрещено');
    }

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param null|string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}