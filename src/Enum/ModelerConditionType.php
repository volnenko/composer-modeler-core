<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerConditionType extends ModelerAbstractEnum
{

    const EQUALS = 'EQUALS';

    const NOT_EQUALS = 'NOT_EQUALS';

    const LESS_THAN = 'LESS_THAN';

    const LESS_EQUALS = 'LESS_EQUALS';

    const GREATER_THAN = 'GREATER_THAN';

    const GREATER_EQUALS = 'GREATER_EQUALS';

    const IS_NULL = 'IS_NULL';

    const NOT_NULL = 'NOT_NULL';

    const CONTAINS = 'CONTAINS';

    const VALUES = array(
        self::NOT_EQUALS,
        self::EQUALS, self::CONTAINS,
        self::LESS_THAN, self::LESS_EQUALS,
        self::GREATER_THAN, self::GREATER_EQUALS,
        self::IS_NULL, self::NOT_NULL,
    );

    /**
     * @return ModelerConditionType[]
     */
    public static function VALUES() {
        return array(
            self::EQUALS(),
            self::NOT_EQUALS(),
            self::CONTAINS(),
            self::LESS_THAN(),
            self::LESS_EQUALS(),
            self::GREATER_THAN(),
            self::GREATER_EQUALS(),
            self::IS_NULL(),
            self::NOT_NULL()
        );
    }

    /**
     * @param $value
     * @return ModelerConditionType|null
     */
    public static function valueOf($value) {
        if (empty($value)) return null;
        if ($value === self::EQUALS) return self::EQUALS();
        if ($value === self::NOT_EQUALS) return self::NOT_EQUALS();
        if ($value === self::CONTAINS) return self::CONTAINS();
        if ($value === self::LESS_THAN) return self::LESS_THAN();
        if ($value === self::LESS_EQUALS) return self::LESS_EQUALS();
        if ($value === self::GREATER_THAN) return self::GREATER_THAN();
        if ($value === self::GREATER_EQUALS) return self::GREATER_EQUALS();
        if ($value === self::IS_NULL) return self::IS_NULL();
        if ($value === self::NOT_NULL) return self::NOT_NULL();
        return null;
    }

    /**
     * @return ModelerConditionType
     */
    public static function EQUALS() {
        return new ModelerConditionType(self::EQUALS, '=');
    }

    /**
     * @return ModelerConditionType
     */
    public static function NOT_EQUALS() {
        return new ModelerConditionType(self::NOT_EQUALS, '!=');
    }

    /**
     * @return ModelerConditionType
     */
    public static function LESS_THAN() {
        return new ModelerConditionType(self::LESS_THAN, '<');
    }

    /**
     * @return ModelerConditionType
     */
    public static function LESS_EQUALS() {
        return new ModelerConditionType(self::LESS_EQUALS, '<=');
    }

    /**
     * @return ModelerConditionType
     */
    public static function GREATER_THAN() {
        return new ModelerConditionType(self::GREATER_THAN, '>');
    }

    /**
     * @return ModelerConditionType
     */
    public static function GREATER_EQUALS() {
        return new ModelerConditionType(self::GREATER_EQUALS, '>=');
    }

    /**
     * @return ModelerConditionType
     */
    public static function IS_NULL() {
        return new ModelerConditionType(self::IS_NULL, 'пустое');
    }

    /**
     * @return ModelerConditionType
     */
    public static function NOT_NULL() {
        return new ModelerConditionType(self::NOT_NULL, 'не пустое');
    }

    /**
     * @return ModelerConditionType
     */
    public static function CONTAINS() {
        return new ModelerConditionType(self::CONTAINS, 'содержит');
    }

    /**
     * ModelerConditionType constructor.
     * @param null|string $code
     * @param null|string $name
     */
    public function __construct($code, $name)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param null|string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}