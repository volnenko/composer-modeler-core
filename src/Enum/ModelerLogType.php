<?php namespace Volnenko\Modeler\Enum;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerLogType extends ModelerAbstractEnum
{

    const INFO = 'INFO';

    const WARN = 'WARN';

    const TRACE = 'TRACE';

    const ERROR = 'ERROR';

    const VALUES = array(self::INFO, self::WARN, self::TRACE, self::ERROR);

    /**
     * @return ModelerLogType[]
     */
    public static function VALUES() {
        return array(
            self::INFO(),
            self::WARN(),
            self::TRACE(),
            self::ERROR()
        );
    }

    /**
     * @return ModelerLogType
     */
    public static function INFO() {
        return new ModelerLogType(self::INFO, self::lc('Info'));
    }

    /**
     * @return ModelerLogType
     */
    public static function WARN() {
        return new ModelerLogType(self::WARN, self::lc('Warn'));
    }

    /**
     * @return ModelerLogType
     */
    public static function TRACE() {
        return new ModelerLogType(self::TRACE, self::lc('Trace'));
    }

    /**
     * @return ModelerLogType
     */
    public static function ERROR() {
        return new ModelerLogType(self::ERROR, self::lc('Error'));
    }

    /**
     * ModelerLogType constructor.
     * @param null|string $code
     * @param null|string $name
     */
    public function __construct($code, $name)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @param $value
     * @return ModelerLogType|null
     */
    public static function valueOf($value) {
        if (empty($value)) return null;
        if ($value === self::INFO) return self::INFO();
        if ($value === self::WARN) return self::WARN();
        if ($value === self::TRACE) return self::TRACE();
        if ($value === self::ERROR) return self::ERROR();
        return null;
    }

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

}