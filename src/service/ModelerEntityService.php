<?php namespace Volnenko\Modeler\Service;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEntityService extends ModelerAbstractEntityService
{

    private static $instance = null;

    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerEntityDAO::getInstance());
        return self::$instance;
    }

}