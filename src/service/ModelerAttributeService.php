<?php namespace Volnenko\Modeler\Service;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerAttributeService extends ModelerAbstractEntityService
{

    private static $instance = null;

    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self(ModelerAttributeDAO::getInstance());
        return self::$instance;
    }

}