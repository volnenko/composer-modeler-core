<?php namespace Volnenko\Modeler\Service;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

abstract class ModelerAbstractService
{

    /**
     * @return ModelerSuccessDTO
     */
    public function ping() {
        return new ModelerSuccessDTO();
    }

    /**
     * @param bool $result
     * @return ModelerResultDTO
     */
    protected function getResult($result) {
        return new ModelerResultDTO($result);
    }

    /**
     * @param Exception $e
     * @return ModelerErrorDTO
     */
    protected function getFail(Exception $e) {
        return new ModelerErrorDTO($e->getMessage());
    }

    /**
     * @return ModelerSuccessDTO
     */
    protected function getSuccess() {
        return new ModelerSuccessDTO();
    }

}