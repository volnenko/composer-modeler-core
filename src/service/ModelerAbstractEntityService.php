<?php namespace Volnenko\Modeler\Service;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerAbstractEntityService extends ModelerAbstractService
{

    /**
     * @var ModelerDataAccessAPI
     */
    private $dao;

    /**
     * ModelerAbstractEntityService constructor.
     * @param ModelerDataAccessAPI $dao
     */
    public function __construct(ModelerDataAccessAPI $dao)
    {
        $this->dao = $dao;
    }

    /**
     * @return ModelerCountDTO
     */
    public function count() {
        try {
            return new ModelerCountDTO($this->dao->count());
        } catch (Exception $e) {
            return new ModelerCountDTO();
        }
    }

    /**
     * @param string $id
     * @return ModelerResultDTO
     */
    public function existsById($id) {
        try {
            return $this->getResult($this->dao->exists($id));
        } catch (Exception $e) {
            return $this->getFail($e);
        }
    }

    /**
     * @return array
     */
    public function ids() {
        try {
            return $this->dao->ids();
        } catch (Exception $e) {
            return array();
        }
    }

    /**
     * @return ModelerErrorDTO|ModelerResultDTO
     */
    public function isEmpty() {
        try {
            return new ModelerResultDTO($this->dao->isEmpty());
        } catch (Exception $e) {
            return $this->getFail($e);
        }
    }

    /**
     * @param string $id
     * @return mixed|null
     */
    public function findOne($id) {
        try {
            return $this->dao->findOne($id);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param int $start
     * @param int $limit
     * @param string $sortAttribute
     * @param string $sortTypeId
     * @return array
     */
    public function findAll($start = null, $limit = null, $sortAttribute = null, $sortTypeId = null) {
        try {
            return $this->dao->findAll($start, $limit, $sortAttribute, $sortTypeId);
        } catch (Exception $e) {
            return array();
        }
    }

    /**
     * @param string $id
     * @return ModelerErrorDTO|ModelerSuccessDTO
     */
    public function removeById($id) {
        try {
            $this->dao->removeById($id);
            return $this->getSuccess();
        } catch (Exception $e) {
            return $this->getFail($e);
        }
    }

    /**
     * @return ModelerErrorDTO|ModelerSuccessDTO
     */
    public function removeAll() {
        try {
            $this->dao->removeAll();
            return $this->getSuccess();
        } catch (Exception $e) {
            return $this->getFail($e);
        }
    }

}