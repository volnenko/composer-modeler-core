<?php namespace Volnenko\Modeler\Util;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerDateTime
{

    public static function timestamp() {
        return round(microtime(true)*1000);
    }

}