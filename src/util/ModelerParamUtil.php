<?php namespace Volnenko\Modeler\Util;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerParamUtil
{

    /**
     * @param string $paramName
     * @return mixed|null
     */
    public static function paramGET($paramName) {
        if (!array_key_exists($paramName, $_GET)) return null;
        return $_GET[$paramName];
    }

    /**
     * @param string $paramName
     * @return mixed|null
     */
    public static function paramPOST($paramName) {
        if (!array_key_exists($paramName, $_POST)) return null;
        return $_POST[$paramName];
    }

    /**
     * @param string $paramName
     * @return bool
     */
    public static function hasPOST($paramName) {
        return array_key_exists($paramName, $_POST);
    }

    /**
     * @return mixed|null
     */
    public static function paramFindValueGET() {
        return self::paramGET(ModelerAttributeConst::FIND_VALUE);
    }

    /**
     * @return mixed|null
     */
    public static function paramFolderIdGET() {
        return self::paramGET(ModelerAttributeConst::FOLDER_ID);
    }

    /**
     * @return mixed|null
     */
    public static function paramIdGET() {
        return self::paramGET(ModelerAttributeConst::ID);
    }

    /**
     * @return mixed|null
     */
    public static function paramUserIdGET() {
        return self::paramGET(ModelerAttributeConst::USER_ID);
    }

    /**
     * @return mixed|null
     */
    public static function paramLocaleIdGET() {
        return self::paramGET(ModelerAttributeConst::LOCALE_ID);
    }

    /**
     * @return mixed|null
     */
    public static function paramLocaleKeyIdGET() {
        return self::paramGET(ModelerAttributeConst::LOCALE_KEY_ID);
    }

    /**
     * @return mixed|null
     */
    public static function paramLocaleKeyValueIdGET() {
        return self::paramGET(ModelerAttributeConst::LOCALE_KEY_VALUE_ID);
    }

    /**
     * @return mixed|null
     */
    public static function paramIdPOST() {
        return self::paramPOST(ModelerAttributeConst::ID);
    }

    /**
     * @return mixed|null
     */
    public static function paramStatisticGET() {
        $statistic = self::paramGET(ModelerAttributeConst::STATISTIC);
        if (empty($statistic)) return ModelerSwitchType::DISABLED;
        return $statistic;
    }

    /**
     * @return string
     */
    public static function paramStatisticInverseGET() {
        $statistic = self::paramStatisticGET();
        return $statistic === ModelerSwitchType::ENABLED ? ModelerSwitchType::DISABLED : ModelerSwitchType::ENABLED;
    }

    /**
     * @return mixed|null
     */
    public static function paramOperationGET() {
        return self::paramGET(ModelerAttributeConst::OPERATION);
    }

    /**
     * @return mixed|null
     */
    public static function paramOperationPOST() {
        return self::paramPOST(ModelerAttributeConst::OPERATION);
    }

    /**
     * @return mixed|null
     */
    public static function paramUserIdPOST() {
        return self::paramPOST(ModelerAttributeConst::USER_ID);
    }

    /**
     * @return bool
     */
    public static function paramOperationSavePOST() {
        return self::paramPOST(ModelerAttributeConst::OPERATION) === ModelerOperationType::SAVE;
    }

    /**
     * @return bool
     */
    public static function paramOperationUploadPOST() {
        return self::paramPOST(ModelerAttributeConst::OPERATION) === ModelerOperationType::UPLOAD;
    }

    /**
     * @return bool
     */
    public static function paramOperationRemoveGET() {
        return self::paramGET(ModelerAttributeConst::OPERATION) === ModelerOperationType::REMOVE;
    }

    /**
     * @return bool
     */
    public static function paramOperationRemovePOST() {
        return self::paramPOST(ModelerAttributeConst::OPERATION) === ModelerOperationType::REMOVE;
    }

    /**
     * @return int|null
     */
    public static function paramLimitGET() {
        return self::paramGET(ModelerAttributeConst::LIMIT);
    }

    /**
     * @return int|null
     */
    public static function paramStartGET() {
        return self::paramGET(ModelerAttributeConst::START);
    }

    /**
     * @return string|null
     */
    public static function paramEnumIdGET() {
        return self::paramGET(ModelerAttributeConst::ENUM_ID);
    }

    /**
     * @return string|null
     */
    public static function paramEnumIdPOST() {
        return self::paramPOST(ModelerAttributeConst::ENUM_ID);
    }

    /**
     * @return string|null
     */
    public static function paramEntityIdPOST() {
        return self::paramPOST(ModelerAttributeConst::ENTITY_ID);
    }

    /**
     * @return string|null
     */
    public static function paramEntityIdGET() {
        return self::paramGET(ModelerAttributeConst::ENTITY_ID);
    }

    /**
     * @return string|null
     */
    public static function paramEntityClassGET() {
        return self::paramGET(ModelerAttributeConst::ENTITY_CLASS);
    }

    /**
     * @return string|null
     */
    public static function paramXTypeGET() {
        return self::paramGET(ModelerAttributeConst::XTYPE);
    }

    /**
     * @param string $default
     * @return bool
     */
    public static function paramSortTypeGET($default = null) {
        $result = self::paramGET(ModelerAttributeConst::SORT_TYPE);
        return empty($result) ? $default : $result;
    }

    /**
     * @param string $default
     * @return bool
     */
    public static function paramSortFieldGET($default = null) {
        $result = self::paramGET(ModelerAttributeConst::SORT_FIELD);
        return empty($result) ? $default : $result;
    }

    /**
     * @return string
     */
    public static function paramSortTypeInverseGET() {
        $sortType = self::paramGET(ModelerAttributeConst::SORT_TYPE);
        return $sortType === ModelerSortType::ASC ? ModelerSortType::DESC : ModelerSortType::ASC;
    }

}