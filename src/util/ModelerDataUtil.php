<?php namespace Volnenko\Modeler\Util;

use Volnenko\Modeler\Constant\ModelerAttributeType;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerDataUtil
{

    /**
     * @param $value
     * @return null|string
     */
    public static function getAttributeTypeCode($value) {
        if ($value === null) return null;
        if (is_string($value)) return ModelerAttributeType::STRING;
        if ($value instanceof DateTime) return ModelerAttributeType::DATE;
        if (is_integer($value)) return ModelerAttributeType::INTEGER;
        if (is_int($value)) return ModelerAttributeType::INTEGER;
        if (is_long($value)) return ModelerAttributeType::LONG;
        if (is_float($value)) return ModelerAttributeType::FLOAT;
        if (is_double($value)) return ModelerAttributeType::DOUBLE;
        if (is_bool($value)) return ModelerAttributeType::BOOLEAN;
        return null;
    }

    /**
     * @param $object
     * @return array
     */
    public static function getMetaByObject($object){
        $result = array();
        foreach ($object as $name => $value) {
            $type = ModelerDataUtil::getAttributeTypeCode($value);
            if ($type === null) continue;
            $result[$name] = $type;
        }
        return $result;
    }

    /**
     * @param String $entityId
     * @return stdClass
     * @throws ModelerAbstractEntityException
     */
    public static function getMetaByEntityId($entityId) {
       $result = new stdClass();
       self::initEntity($result);
       $entityAttributes = ModelerEntityAttributeDAO::getInstance()->findAllByEntity($entityId, null, null, ModelerSortType::ASC, ModelerAttributeConst::ORDER_INDEX);
       foreach ($entityAttributes as $entityAttribute) {
           $attributeId = $entityAttribute->attributeId;
           $attribute = ModelerAttributeDAO::getInstance()->findOne($attributeId);
           if ($attribute === null) continue;
           $attributeTypeId = $attribute->getAttributeTypeId();
           if (empty($attributeTypeId)) continue;
           $attributeCode = $attribute[ModelerAttributeConst::CODE];
           $result[$attributeCode] = $attributeTypeId;
       }
       return $result;
    }

    /**
     * @param $object
     * @param string $dataJSON
     * @param string $metaJSON
     * @return null
     */
    public static function loadRowData($object, $dataJSON, $metaJSON) {
        if (empty($object)) return null;
        if (empty($dataJSON)) return null;
        if (empty($metaJSON)) return null;
        $dataObject = json_decode($dataJSON);
        $metaObject = json_decode($metaJSON);
        foreach ($dataObject as $name => $value) {
            $type = $metaObject->$name;
            if (empty($type)) continue;
            $object->$name = $value;
            //TODO Implement parse DateTime
            //echo $name.':'.$type.':'.$this->$name."\n";
        }
        return $object;
    }

    /**
     * @param $entity
     * @return mixed
     */
    public static function initEntity($entity) {
        if (empty($entity->id)) $entity->id = ModelerUUID::uuid();
        if (empty($entity->version)) $entity->version = 0;
        if (empty($entity->created)) $entity->created = new DateTime();
        if (empty($entity->updated)) $entity->updated = new DateTime();
        if (empty($entity->orderIndex)) $entity->orderIndex = ModelerDateTime::timestamp();
        return $entity;
    }

    /**
     *
     */
    public static function createRaw() {
        $value = new stdClass();
        return self::initEntity($value);
    }

    /**
     * @param $object
     * @return string
     */
    public static function objectToJson($object) {
        $result = array();
        foreach ($object as $name => $value) {
            if ($value === null) continue;
            if ($value instanceof DateTime) {
                $result[$name] = $value->format('c');
                continue;
            }
            $check = is_numeric($value) || is_bool($value) || is_string($value);
            if (!$check) continue;
            $result[$name] = $value;
        }
        return json_encode($result, JSON_PRETTY_PRINT);
    }

    /**
     * @param $object
     * @return string
     */
    public static function objectToMetaJson($object) {
        $metaObject = self::getMetaByObject($object);
        return json_encode($metaObject, JSON_PRETTY_PRINT);
    }

}