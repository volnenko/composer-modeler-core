<?php namespace Volnenko\Modeler\Util;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerRecordUtil
{

    /**
     * @param $records
     * @param string $id
     * @return null
     */
    public static function getNextId($records, $id) {
        if ($records == null || empty($records)) return null;
        if ($id == null || empty($id)) return null;

    }

    /**
     * @param $records
     * @param string $id
     * @return null
     */
    public static function getBeforeId($records, $id) {
        if ($records == null || empty($records)) return null;
        if ($id == null || empty($id)) return null;

    }

}