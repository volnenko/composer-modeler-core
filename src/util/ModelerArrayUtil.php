<?php namespace Volnenko\Modeler\Util;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerArrayUtil
{

    /**
     * @param $array
     * @param int $start
     * @param int $limit
     * @return array
     */
    public static function paging($array, $start = null, $limit = null) {
        if (count($array) === 0) return $array;
        if (is_int($start) && is_int($limit)) return array_slice($array, $start, $limit);
        if (is_int($start) && !is_int($limit)) return array_slice($array, $start);
        if (!is_int($start) && is_int($limit)) return array_slice($array, 0, $limit);
        return $array;
    }

}