<?php namespace Volnenko\Modeler\Util;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerServiceUtil
{

    /**
     * @param mixed $value
     */
    public static function printJSON($value) {
        echo json_encode($value, JSON_PRETTY_PRINT);
    }

    /**
     * @param bool $result
     */
    public static function printResultJSON($result) {
        self::printJSON(new ModelerResultDTO($result));
    }

    public static function printSuccessJSON() {
        self::printResultJSON(true);
    }

    public static function printFailJSON() {
        self::printResultJSON(false);
    }

    /**
     * @param int $count
     */
    public static function printCountJSON($count = 0) {
        self::printJSON(new ModelerCountDTO($count));
    }

    /**
     * @param Exception $e
     */
    public static function printExceptionJSON(Exception $e) {
        self::printJSON(new ModelerErrorDTO($e->getMessage()));
    }

    public static function printEmptyArrayJSON() {
        self::printJSON(array());
    }

}