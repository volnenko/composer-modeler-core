<?php namespace Volnenko\Modeler\Util;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerStringUtil
{

    public static function prepare($value) {
        return empty($value) ? '' : $value;
    }

    public static function getKeyValue($object, $key) {
        if (property_exists($object, $key)) {
            return self::prepare($object->$key);
        } else {
            return '';
        }
    }

}