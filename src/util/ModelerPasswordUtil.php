<?php namespace Volnenko\Modeler\Util;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerPasswordUtil
{

    /**
     * @param string|null $password
     * @return string|null
     */
    public static function getPasswordHash($password)
    {
        if ($password === null) return null;
        return sha1($password);
    }

}