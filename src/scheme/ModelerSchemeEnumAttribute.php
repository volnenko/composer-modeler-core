<?php namespace Volnenko\Modeler\Scheme;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSchemeEnumAttribute extends ModelerAbstractSchemeComponent
{

    /**
     * @var ModelerSchemeEnum
     */
    private $enum;

    /**
     * @var string
     */
    private $code;

    /**
     * ModelerSchemeEnumAttribute constructor.
     * @param ModelerScheme $scheme
     * @param ModelerSchemeEnum $enum
     * @param string $code
     */
    public function __construct($scheme, $enum, $code)
    {
        parent::__construct($scheme);
        $this->enum = $enum;
        $this->code = $code;
    }

    /**
     * @param $code
     * @param $name
     * @return ModelerSchemeEnumValue
     */
    public function enumValue($code, $name) {
        return $this->enum->enumValue($code, $name);
    }

    /**
     * @param $code
     * @return ModelerSchemeEnumAttribute
     */
    public function enumAttribute($code) {
        return $this->enum->enumAttribute($code);
    }

    /**
     * @return ModelerSchemeEnumAttribute
     * @throws ModelerAbstractEntityException
     */
    public function flush()
    {
        ModelerSchemeManager::getInstance()->mergeEnumAttribute($this->enum->code(), $this->code);
        return $this;
    }

}