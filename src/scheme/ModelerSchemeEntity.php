<?php namespace Volnenko\Modeler\Scheme;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSchemeEntity extends ModelerAbstractSchemeComponent
{

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    var $supportSystem = false;

    /**
     * @var array
     */
    private $entityAttributes = array();

    /**
     * ModelerSchemeEntity constructor.
     * @param $scheme ModelerScheme
     * @param string $code
     * @param string $name
     */
    public function __construct($scheme, $code, $name)
    {
        parent::__construct($scheme);
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @param string $code
     * @return ModelerSchemeEntityAttribute
     */
    public function entityAttribute($code) {
        $entityAttribute = new ModelerSchemeEntityAttribute($this->scheme, $this, $code);
        $this->entityAttributes[] = $entityAttribute;
        return $entityAttribute;
    }

    /**
     * @param bool $supportSystem
     * @return ModelerSchemeEntity
     */
    public function supportSystem($supportSystem = true) {
        $this->supportSystem = $supportSystem;
        return $this;
    }

    /**
     * @return array
     */
    public function entityAttributes()
    {
        return $this->entityAttributes;
    }

    /**
     * @return string
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return ModelerSchemeEntity
     * @throws ModelerAbstractEntityException
     */
    public function flush()
    {
        ModelerSchemeManager::getInstance()->mergeEntity($this->code, $this->name, $this->supportSystem);
        foreach ($this->entityAttributes as $entityAttribute) $entityAttribute->flush();
        return $this;
    }

}