<?php namespace Volnenko\Modeler\Scheme;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSchemeEnumValueAttribute extends ModelerAbstractSchemeComponent
{

    /**
     * @var ModelerSchemeEnum
     */
    private $enum;

    /**
     * @var ModelerSchemeEnumValue
     */
    private $enumValue;

    /**
     * @var string
     */
    private $code;

    /**
     * @var mixed
     */
    private $value;

    /**
     * ModelerSchemeEnumValueAttribute constructor.
     * @param ModelerScheme $scheme
     * @param ModelerSchemeEnum $enum
     * @param ModelerSchemeEnumValue $enumValue
     * @param string $code
     * @param mixed $value
     */
    public function __construct($scheme, $enum, $enumValue, $code, $value)
    {
        parent::__construct($scheme);
        $this->enum = $enum;
        $this->enumValue = $enumValue;
        $this->code = $code;
        $this->value = $value;
    }

    /**
     * @param $code
     * @param $name
     * @return ModelerSchemeEnumValue
     */
    public function enumValue($code, $name) {
        return $this->enum->enumValue($code, $name);
    }

    /**
     * @param $code
     * @return ModelerSchemeEnumAttribute
     */
    public function enumAttribute($code) {
        return $this->enum->enumAttribute($code);
    }

    /**
     * @param string $code
     * @param mixed $value
     * @return ModelerSchemeEnumValueAttribute
     */
    public function enumValueAttribute($code, $value) {
        return $this->enumValue->enumValueAttribute($code, $value);
    }

    /**
     * @return string
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * @return ModelerSchemeEnumValueAttribute
     * @throws ModelerAbstractEntityException
     */
    public function flush()
    {
        ModelerSchemeManager::getInstance()->mergeEnumValueAttribute(
            $this->enum->code(), $this->enumValue->code(), $this->code(), $this->value()
        );
        return $this;
    }

}