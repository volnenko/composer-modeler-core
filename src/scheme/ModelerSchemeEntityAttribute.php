<?php namespace Volnenko\Modeler\Scheme;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSchemeEntityAttribute extends ModelerAbstractSchemeComponent
{

    /**
     * @var ModelerSchemeEntity
     */
    private $entity;

    /**
     * @var string
     */
    private $code;

    /**
     * ModelerSchemeEntityAttribute constructor.
     * @param ModelerScheme $scheme
     * @param ModelerSchemeEntity $entity
     * @param string $code
     */
    public function __construct($scheme, $entity, $code)
    {
        parent::__construct($scheme);
        $this->code = $code;
        $this->entity = $entity;
    }

    /**
     * @param string $code
     * @return ModelerSchemeEntityAttribute
     */
    public function entityAttribute($code) {
        return $this->entity->entityAttribute($code);
    }

    /**
     * @return string
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * @return ModelerSchemeEntityAttribute
     * @throws ModelerAbstractEntityException
     */
    public function flush()
    {
        ModelerSchemeManager::getInstance()->mergeEntityAttribute($this->entity->code(), $this->code);
        return $this;
    }

}