<?php namespace Volnenko\Modeler\Scheme;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

abstract class ModelerAbstractSchemeComponent implements ModelerSchemeAPIComponent
{

    /**
     * @var ModelerScheme
     */
    protected $scheme;

    public function __construct($scheme)
    {
        $this->scheme = $scheme;
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeString($code, $name)
    {
        return $this->scheme->attributeString($code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeInteger($code, $name)
    {
        return $this->scheme->attributeInteger($code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeLong($code, $name)
    {
        return $this->scheme->attributeLong($code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeDouble($code, $name)
    {
        return $this->scheme->attributeDouble($code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeFloat($code, $name)
    {
        return $this->scheme->attributeFloat($code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeBoolean($code, $name)
    {
        return $this->scheme->attributeBoolean($code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeDateTime($code, $name)
    {
        return $this->scheme->attributeDateTime($code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeEntity
     */
    public function entity($code, $name)
    {
        return $this->scheme->entity($code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeEnum
     */
    public function enum($code, $name)
    {
        return $this->scheme->enum($code, $name);
    }

    /**
     * @return ModelerScheme
     */
    public function scheme()
    {
        return $this->scheme;
    }

    public function store()
    {
        return $this->scheme->store();
    }

    public function system()
    {
        return $this->scheme->system();
    }

}