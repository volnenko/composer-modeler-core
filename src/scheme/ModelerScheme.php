<?php namespace Volnenko\Modeler\Scheme;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerScheme implements ModelerSchemeAPI
{

    /**
     * @var ModelerScheme
     */
    private static $instance = null;

    /**
     * @return ModelerScheme
     */
    public static function getInstance()
    {
        if (null === self::$instance) self::$instance = new self();
        return self::$instance;
    }

    /**
     * @var array
     */
    private $attributes = array();

    /**
     * @var array
     */
    private $entities = array();

    /**
     * @var array
     */
    private $enums = array();

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeString($code, $name)
    {
        return $this->attribute(ModelerAttributeType::STRING, $code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeInteger($code, $name)
    {
        return $this->attribute(ModelerAttributeType::INTEGER, $code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeLong($code, $name)
    {
        return $this->attribute(ModelerAttributeType::LONG, $code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeDouble($code, $name)
    {
        return $this->attribute(ModelerAttributeType::DOUBLE, $code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeFloat($code, $name)
    {
        return $this->attribute(ModelerAttributeType::FLOAT, $code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeBoolean($code, $name)
    {
        return $this->attribute(ModelerAttributeType::BOOLEAN, $code, $name);
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    public function attributeDateTime($code, $name)
    {
        return $this->attribute(ModelerAttributeType::DATE, $code, $name);
    }

    /**
     * @param string $attributeTypeId
     * @param string $code
     * @param string $name
     * @return ModelerSchemeAttribute
     */
    private function attribute($attributeTypeId, $code, $name) {
        $attribute = new ModelerSchemeAttribute($this, $code, $name, $attributeTypeId);
        $this->attributes[] = $attribute;
        return $attribute;
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeEntity
     */
    public function entity($code, $name) {
        $entity = new ModelerSchemeEntity($this, $code, $name);
        $this->entities[] = $entity;
        return $entity;
    }

    /**
     * @param string $code
     * @param string $name
     * @return ModelerSchemeEnum
     */
    public function enum($code, $name) {
        $enum = new ModelerSchemeEnum($this, $code, $name);
        $this->enums[] = $enum;
        return $enum;
    }

    public function system() {
        foreach ($this->attributes as $attribute) $attribute->supportSystem();
        foreach ($this->entities as $entity) $entity->supportSystem();
        foreach ($this->enums as $enum) $enum->supportSystem();
        return $this;
    }

    /**
     * @return $this
     */
    public function store() {
        foreach ($this->attributes as $attribute) $attribute->flush();
        foreach ($this->entities as $entity) $entity->flush();
        foreach ($this->enums as $enum) $enum->flush();
        return $this;
    }

}