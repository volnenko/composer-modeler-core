<?php namespace Volnenko\Modeler\Scheme;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSchemeEnum extends ModelerAbstractSchemeComponent
{

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    var $supportSystem = false;

    /**
     * @var array
     */
    private $enumValues = array();

    /**
     * @var array
     */
    private $enumAttributes = array();

    /**
     * ModelerSchemeEnum constructor.
     * @param $scheme ModelerScheme
     * @param string $code
     * @param string $name
     */
    public function __construct($scheme, $code, $name)
    {
        parent::__construct($scheme);
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @param $code
     * @param $name
     * @return ModelerSchemeEnumValue
     */
    public function enumValue($code, $name) {
        $enumValue = new ModelerSchemeEnumValue($this->scheme, $this, $code, $name);
        $this->enumValues[] = $enumValue;
        return $enumValue;
    }

    /**
     * @param $code
     * @return ModelerSchemeEnumAttribute
     */
    public function enumAttribute($code) {
        $enumAttribute = new ModelerSchemeEnumAttribute($this->scheme, $this, $code);
        $this->enumAttributes[] = $enumAttribute;
        return $enumAttribute;
    }

    /**
     * @return string
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function enumValues()
    {
        return $this->enumValues;
    }

    /**
     * @return array
     */
    public function enumAttributes()
    {
        return $this->enumAttributes;
    }

    /**
     * @param bool $supportSystem
     * @return ModelerSchemeEnum
     */
    public function supportSystem($supportSystem = true) {
        $this->supportSystem = $supportSystem;
        foreach ($this->enumValues as $enumValue) $enumValue->supportSystem($supportSystem);
        return $this;
    }

    /**
     * @return ModelerSchemeEnum
     * @throws ModelerAbstractEntityException
     */
    public function flush()
    {
        ModelerSchemeManager::getInstance()->mergeEnum($this->code, $this->name, $this->supportSystem);
        foreach ($this->enumValues as $enumValue) $enumValue->flush();
        foreach ($this->enumAttributes as $enumAttribute) $enumAttribute->flush();
        return $this;
    }

}