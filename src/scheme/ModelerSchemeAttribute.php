<?php namespace Volnenko\Modeler\Scheme;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSchemeAttribute extends ModelerAbstractSchemeComponent
{

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    var $supportSystem = false;

    /**
     * @var string
     */
    private $attributeTypeId;

    /**
     * ModelerSchemeAttribute constructor.
     * @param ModelerScheme $scheme
     * @param string $code
     * @param string $name
     * @param string $attributeTypeId
     */
    public function __construct($scheme, $code, $name, $attributeTypeId)
    {
        parent::__construct($scheme);
        $this->code = $code;
        $this->name = $name;
        $this->attributeTypeId = $attributeTypeId;
    }

    /**
     * @param bool $supportSystem
     * @return ModelerSchemeAttribute
     */
    public function supportSystem($supportSystem = true) {
        $this->supportSystem = $supportSystem;
        return $this;
    }

    /**
     * @return ModelerSchemeAttribute
     * @throws ModelerAbstractEntityException
     */
    public function flush()
    {
        ModelerSchemeManager::getInstance()->mergeAttribute(
            $this->code, $this->name, $this->attributeTypeId, $this->supportSystem
        );
        return $this;
    }

}