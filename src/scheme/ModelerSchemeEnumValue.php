<?php namespace Volnenko\Modeler\Scheme;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerSchemeEnumValue extends ModelerAbstractSchemeComponent
{

    /**
     * @var ModelerSchemeEnum
     */
    private $enum;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    var $supportSystem = false;

    /**
     * @var array
     */
    private $enumValueAttributes = array();

    /**
     * ModelerSchemeEnumValue constructor.
     * @param ModelerScheme $scheme
     * @param ModelerSchemeEnum $enum
     * @param string $code
     * @param string $name
     */
    public function __construct($scheme, $enum, $code, $name)
    {
        parent::__construct($scheme);
        $this->enum = $enum;
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @param string $code
     * @param mixed $value
     * @return ModelerSchemeEnumValueAttribute
     */
    public function enumValueAttribute($code, $value) {
        $enumValueAttribute = new ModelerSchemeEnumValueAttribute($this->scheme, $this->enum, $this, $code, $value);
        $this->enumValueAttributes[] = $enumValueAttribute;
        return $enumValueAttribute;
    }

    /**
     * @param $code
     * @param $name
     * @return ModelerSchemeEnumValue
     */
    public function enumValue($code, $name) {
        return $this->enum->enumValue($code, $name);
    }

    /**
     * @param $code
     * @return ModelerSchemeEnumAttribute
     */
    public function enumAttribute($code) {
        return $this->enum->enumAttribute($code);
    }

    /**
     * @return array
     */
    public function enumValueAttributes()
    {
        return $this->enumValueAttributes;
    }

    /**
     * @param bool $supportSystem
     * @return ModelerSchemeEnumValue
     */
    public function supportSystem($supportSystem = true) {
        $this->supportSystem = $supportSystem;
        return $this;
    }

    /**
     * @return ModelerSchemeEnumValue
     * @throws ModelerAbstractEntityException
     */
    public function flush()
    {
        ModelerSchemeManager::getInstance()->mergeEnumValue(
            $this->enum->code(), $this->code, $this->name, $this->supportSystem
        );
        foreach ($this->enumValueAttributes as $enumValueAttribute) $enumValueAttribute->flush();
        return $this;
    }

}