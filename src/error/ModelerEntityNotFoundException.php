<?php namespace Volnenko\Modeler\Error;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

class ModelerEntityNotFoundException extends ModelerAbstractEntityException
{

}