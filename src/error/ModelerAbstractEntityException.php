<?php namespace Volnenko\Modeler\Error;

/**
 * @author Denis Volnenko <denis@volnenko.ru>
 */

abstract class ModelerAbstractEntityException extends ModelerAbstractException
{

}