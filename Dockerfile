FROM epcallan/php7-testing-phpunit:7.2-phpunit7
ADD ./ /app
WORKDIR /app
RUN phpunit --configuration ./config.xml